import {Decimal} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/common/common_types_pb";
import {SampleArrayValueMsg} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/samplearrayvalue_pb";
import {
    NumericMetricValueMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/numericmetricvalue_pb";
import {
    StringMetricValueMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/stringmetricvalue_pb";
import {
    AlertPair,
    AlertUpdate,
    ComponentPair,
    ComponentUpdate,
    ContextPair,
    ContextUpdate,
    MetricPair,
    MetricUpdate,
    OperationPair,
    OperationUpdate
} from "../gen/org/protosdc/ui/model/backend/mdib/mdib_types_pb";
import {AbstractStateMsg} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/abstractstate_pb";
import {
    EnsembleContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/ensemblecontextstate_pb";
import {
    LocationContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/locationcontextstate_pb";
import {
    MeansContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/meanscontextstate_pb";
import {
    WorkflowContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/workflowcontextstate_pb";
import {
    PatientContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/patientcontextstate_pb";
import {
    OperatorContextStateMsg
} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/operatorcontextstate_pb";


/// since there is no BigDecimal equivalent and we do not want to go to float because of precision loss,
/// this does string magic. Use when only displaying the value. For (lossy) calculations, use convertDecimalToFloat
export function convertDecimalToString(decimal?: Decimal): string | undefined {
    if (decimal === undefined || decimal === null) {
        return
    }
    if (decimal.scale !== 0) {
        const asString = decimal.value.toString()
        const leftIdx = asString.length - decimal.scale
        return [asString.slice(0, leftIdx), ".", asString.slice(leftIdx)].join('')
    }
    return decimal.value.toString()
}

export function convertDecimalToFloat(decimal?: Decimal): number | undefined {
    if (!decimal) {
        return undefined
    }
    if (decimal.scale !== 0) {
        return Number(decimal.value) * (10 ** (-decimal.scale))
    }
    return Number(decimal.value)
}

export function metricDeterminationTime(metricValue?: SampleArrayValueMsg | NumericMetricValueMsg | StringMetricValueMsg): bigint | undefined {
    return metricValue?.abstractMetricValue?.determinationTimeAttr?.unsignedLong
}


export function getHandle(abstractState?: AbstractStateMsg) {
    return abstractState?.descriptorHandleAttr?.string
}

export function getMetricUpdateDescriptorHandle(metricUpdate: MetricUpdate) {
    switch (metricUpdate.state.case) {
        case "numericMetricStateMsg":
        case "stringMetricStateMsg":
        case "distributionSampleArrayMetricStateMsg":
        case "realTimeSampleArrayMetricStateMsg": {
            return getHandle(metricUpdate.state.value.abstractMetricState?.abstractState)
        }
        case "enumStringMetricStateMsg": {
            return getHandle(metricUpdate.state.value.stringMetricState?.abstractMetricState?.abstractState)
        }
        case undefined: {
            console.error("Undefined metric type in update")
            return undefined
        }
    }
}

export function getAlertUpdateDescriptorHandle(alertUpdate: AlertUpdate) {
    switch (alertUpdate.state.case) {
        case "alertConditionStateMsg":
        case "alertSystemStateMsg":
        case "alertSignalStateMsg": {
            return getHandle(alertUpdate.state.value.abstractAlertState?.abstractState)
        }
        case "limitAlertConditionStateMsg": {
            return getHandle(alertUpdate.state.value.alertConditionState?.abstractAlertState?.abstractState)
        }
        case undefined: {
            console.error("Undefined alert type in update")
            return undefined
        }
    }
}

export function getComponentUpdateDescriptorHandle(componentUpdate: ComponentUpdate) {
    switch (componentUpdate.state.case) {
        case "batteryStateMsg":
        case "channelStateMsg":
        case "clockStateMsg":
        case "scoStateMsg":
        case "systemContextStateMsg": {
            return getHandle(componentUpdate.state.value.abstractDeviceComponentState?.abstractState)
        }

        case "mdsStateMsg":
        case "vmdStateMsg": {
            return getHandle(componentUpdate.state.value.abstractComplexDeviceComponentState?.abstractDeviceComponentState?.abstractState)
        }
        case undefined: {
            console.error("Undefined component type in update")
            return undefined
        }
    }
}

export function getContextUpdateDescriptorHandle(contextUpdate: ContextUpdate) {
    return getHandle(contextUpdate.state.value?.abstractContextState?.abstractMultiState?.abstractState)
}

export function getContextStateStateHandle(contextState: EnsembleContextStateMsg | LocationContextStateMsg | MeansContextStateMsg | WorkflowContextStateMsg | PatientContextStateMsg | OperatorContextStateMsg) {
    return contextState.abstractContextState?.abstractMultiState?.handleAttr?.string
}

export function getContextUpdateStateHandle(contextUpdate: ContextUpdate) {
    return getContextStateStateHandle(contextUpdate.state.value!!)
}

export function getOperationUpdateDescriptorHandle(operationUpdate: OperationUpdate) {
    return getHandle(operationUpdate.state.value?.abstractOperationState?.abstractState)
}

export function getMetricPairDescriptorHandle(pair: MetricPair) {
    switch (pair.pair.case) {
        case "numericMetricPair":
        case "stringMetricPair":
        case "realTimeSampleArrayMetricPair":
        case "distributionSampleArrayMetricPair": {
            return pair.pair.value.descriptor?.abstractMetricDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case "enumStringMetricPair": {
            return pair.pair.value.descriptor?.stringMetricDescriptor?.abstractMetricDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case undefined: {
            console.error("Undefined metric type in update")
            return undefined
        }
    }
}

export function getComponentPairDescriptorHandle(pair: ComponentPair) {
    switch (pair.pair.case) {
        case "mdsPair":
        case "vmdPair": {
            return pair.pair.value.descriptor?.abstractComplexDeviceComponentDescriptor?.abstractDeviceComponentDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case "systemContextPair":
        case "batteryPair":
        case "channelPair":
        case "clockPair":
        case "scoPair": {
            return pair.pair.value.descriptor?.abstractDeviceComponentDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case undefined: {
            console.error("Undefined component type in update")
            return undefined
        }
    }
}

export function getAlertPairDescriptorHandle(pair: AlertPair) {
    switch (pair.pair.case) {
        case "alertConditionPair":
        case "alertSystemPair":
        case "alertSignalPair": {
            return pair.pair.value.descriptor?.abstractAlertDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case "limitAlertConditionPair": {
            return pair.pair.value.descriptor?.alertConditionDescriptor?.abstractAlertDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case undefined: {
            console.error("Undefined alert type in update")
            return undefined
        }
    }
}

export function getContextPairDescriptorHandle(pair: ContextPair) {
    return pair.pair.value?.descriptor?.abstractContextDescriptor?.abstractDescriptor?.handleAttr?.string
}

export function getOperationPairDescriptorHandle(pair: OperationPair) {
    switch (pair.pair.case) {
        case "activateOperationPair":
        case "setAlertStateOperationPair":
        case "setComponentStateOperationPair":
        case "setContextStateOperationPair":
        case "setMetricStateOperationPair": {
            return pair.pair.value.descriptor?.abstractSetStateOperationDescriptor?.abstractOperationDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case "setStringOperationPair":
        case "setValueOperationPair": {
            return pair.pair.value.descriptor?.abstractOperationDescriptor?.abstractDescriptor?.handleAttr?.string
        }
        case undefined: {
            console.error("Undefined operation type in update")
            return undefined
        }
    }
}