import {Crypto} from "../gen/org/protosdc/ui/model/frontend/frontend_types_pb";

export class CertificateInfo {
    constructor(
        public privateKeyData: Uint8Array | null,
        public publicKeyData: Uint8Array | null,
        public caCertData: Uint8Array | null,
        public privateKeyPassword?: string | undefined
    ) {
    }

    isUsable(): Boolean {
        return !((this.publicKeyData == null || !this.publicKeyData.length)
            || (this.privateKeyData == null || !this.publicKeyData.length)
            || (this.caCertData == null || !this.caCertData.length))
    }

    toProto(): Crypto {
        if (!this.isUsable()) {
            console.error("Creating unusable proto crypto")
        }
        let cert = new Crypto()
        cert.participantPublic = this.publicKeyData ?? new Uint8Array(0);
        cert.participantPrivate = this.privateKeyData ?? new Uint8Array(0);
        cert.caCert = this.caCertData ?? new Uint8Array(0);
        cert.participantPrivatePassword = this.privateKeyPassword ?? undefined;
        return cert;
    }
}

export async function loadFile(event: Event, setter: (a: Uint8Array) => void) {
    const target = event.target as HTMLInputElement;
    if (!target.files?.length)
        return;

    const file = target.files[0] as File;
    const reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = function (evt) {
        console.log(typeof evt.target?.result);

        if (evt.target) {
            // our kinds of files are always arraybuffers
            const result = evt.target.result!! as ArrayBuffer
            const loaded = new Uint8Array(result);
            console.log("Loaded (len " + loaded.length + ")")
            setter(loaded)
        }

    }
}