import {ConnectionState} from "./gen/org/protosdc/ui/model/common/common_types_pb";

export enum ConnectionStatus {
    Disconnected = "DISCONNECTED",
    Disconnecting = "DISCONNECTING",
    Connecting = "CONNECTING",
    Connected = "CONNECTED"
}

export function fromProtoConnectionStatus(c?: ConnectionState): ConnectionStatus | undefined {
    if (c == undefined) {
        console.log("null c")
        return undefined
    }

    console.log(c)

    switch (c) {
        case ConnectionState.CONNECTED:
            return ConnectionStatus.Connected
        case ConnectionState.DISCONNECTED:
            return ConnectionStatus.Disconnected
        case ConnectionState.CONNECTING:
            return ConnectionStatus.Connecting
        case ConnectionState.DISCONNECTING:
            return ConnectionStatus.Disconnecting
        case ConnectionState.UNSPECIFIED:
            console.error("Unspecified connection status")
            return ConnectionStatus.Disconnected
    }
}

export enum ContentView {
    Home = "home",
}

export enum DeviceTab {
    Metric = "metric",
    Waveform = "waveform",
    MdibTree = "mdibtree"
}