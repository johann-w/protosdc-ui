declare module "*.vue" {
    import {defineComponent} from "vue";
    const component: ReturnType<typeof defineComponent>;
    export default component;

}
declare module "*.cjs" {
    const device_names: any;
    export default device_names;
}
