import {createApp} from "vue";
import App from "./App.vue";
import "./app.css";
import {parse, stringify} from 'smol-toml'

const app = createApp(App);
fetch("config/config.toml")
    .then((response) => response.text())
    .then((response) => parse(response))
    .then((config) => {

        console.log("Loaded config: " + stringify(config))

        // either use window.config
        // window.config = config
        // or use [Vue Global Config][1]
        app.config.globalProperties.config = config
        // FINALLY, mount the app
        app.mount("#app")
    })

// app.mount("#app");