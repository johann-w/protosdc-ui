import {reactive} from "vue";
import {Device} from "./device.js";
import {ContentView} from "./enums";
import {CertificateInfo} from "./utilities/certificates";

export default reactive({
    backend_address: "http://127.0.0.1:50053",
    devices: {} as { [key: string]: Device },
    content_view: ContentView.Home as ContentView | String,
    certs: new CertificateInfo(
        null,
        null,
        null,
        undefined
    ) as CertificateInfo,
    enableTls: false
});
