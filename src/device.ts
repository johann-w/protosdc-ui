import {ConnectionStatus} from "./enums";
import {
    AlertPair,
    ComponentPair,
    ContextPair,
    MetricPair,
    OperationPair
} from "./gen/org/protosdc/ui/model/backend/mdib/mdib_types_pb";
import {MdibVersionGroupMsg} from "@sdc-suite/protosdc-model/org/somda/protosdc/proto/model/biceps/mdibversiongroup_pb";

export interface Device {
    name: string;
    uuid: string;
    ip: string[];
    adapterIp: string;
    eprAddress: string;
    connectionStatus: ConnectionStatus;
    mdib: Mdib;
}

export interface Mdib {
    metrics: { [key: string]: MetricPair };
    alerts: { [key: string]: AlertPair };
    components: { [key: string]: ComponentPair };
    contexts: { [key: string]: ContextPair };
    operations: { [key: string]: OperationPair };
    version?: MdibVersionGroupMsg;
}