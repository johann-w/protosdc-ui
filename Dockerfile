FROM node:18
RUN apt update
RUN apt install -y protobuf-compiler  libsoup2.4-dev libwebkit2gtk-4.0-dev xvfb fuse libfuse2 libgbm-dev libgl1-mesa-dev python3 python3-pip python3-venv  libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rust.sh
RUN bash rust.sh -y
