
mkdir -p src/gen

protoc -I rust/api/src/ -I rust/api/dummy_files/ \
--plugin $(realpath ./node_modules/.bin/protoc-gen-es) \
--plugin $(realpath ./node_modules/.bin/protoc-gen-connect-es) \
--es_out src/gen/ --es_opt target=ts \
--es_opt rewrite_imports=**/*/proto/model/**/*_pb.js:@sdc-suite/protosdc-model \
--connect-es_out src/gen --connect-es_opt target=ts \
 $(find rust/api/src -name '*.proto' -follow)