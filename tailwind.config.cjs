module.exports = {
  content: ["./src/**/*.{vue,js,ts}"],
  daisyui: {
    styled: true,
    themes: true,
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: "",
    darkTheme: "dark",
    themes: [
      "dark"
    ],
  },
  plugins: [require("daisyui"),require('@tailwindcss/typography'),],
};
