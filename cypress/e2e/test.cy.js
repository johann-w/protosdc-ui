describe("Discover, connect and disconnect", () => {
  beforeEach(() => {
    // Visit the website before each test
    cy.visit("http://127.0.0.1:8000");
  });

  it("Click discover", () => {
    cy.wait(10000)
    // Find all buttons on the page
    cy.get("button").contains("button", "Discover").click();
    cy.wait(2000);
    cy.get('div.indicator input[type="text"]').should('be.visible').type('Test Device');
    cy.get("button").contains("Connect").click();
    cy.wait(5000)
    cy.get("button").contains("Disconnect").click();
  });
});
