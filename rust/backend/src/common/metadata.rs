use dpws::xml::parsing::dpws::common::LocalizedStringType;
use dpws::xml::parsing::dpws::this::{ThisDevice, ThisModel};
use protosdc_proto::metadata::EndpointMetadata;

#[derive(Clone, Debug, Default, serde::Serialize)]
pub struct DeviceMetadata {
    pub friendly_name: Vec<LocalizedString>,
    pub firmware_version: Option<String>,
    pub hardware_version: Option<String>,
    pub software_version: Option<String>,
    pub serial_number: Option<String>,
    pub manufacturer: Vec<LocalizedString>,
    pub manufacturer_url: Option<String>,
    pub model_name: Vec<LocalizedString>,
    pub model_url: Option<String>,
}

impl From<(Option<ThisModel>, Option<ThisDevice>)> for DeviceMetadata {
    fn from((model, device): (Option<ThisModel>, Option<ThisDevice>)) -> Self {
        Self {
            friendly_name: device
                .as_ref()
                .map(|it| {
                    it.friendly_name
                        .iter()
                        .map(|it| it.clone().into())
                        .collect()
                })
                .unwrap_or_default(),
            firmware_version: device
                .as_ref()
                .map(|it| it.firmware_version.clone())
                .unwrap_or_default(),
            hardware_version: None,
            software_version: None,
            serial_number: device
                .as_ref()
                .map(|it| it.serial_number.clone())
                .unwrap_or_default(),
            manufacturer: model
                .as_ref()
                .map(|it| it.manufacturer.iter().map(|it| it.clone().into()).collect())
                .unwrap_or_default(),
            manufacturer_url: model
                .as_ref()
                .map(|it| it.manufacturer_url.clone())
                .unwrap_or_default(),
            model_name: model
                .as_ref()
                .map(|it| it.model_name.iter().map(|it| it.clone().into()).collect())
                .unwrap_or_default(),
            model_url: model
                .as_ref()
                .map(|it| it.model_url.clone())
                .unwrap_or_default(),
        }
    }
}

impl From<EndpointMetadata> for DeviceMetadata {
    fn from(data: EndpointMetadata) -> Self {
        Self {
            friendly_name: data.friendly_name.into_iter().map(|it| it.into()).collect(),
            firmware_version: data.firmware_version,
            hardware_version: data.hardware_version,
            software_version: data.software_version,
            serial_number: data.serial_number,
            manufacturer: data.manufacturer.into_iter().map(|it| it.into()).collect(),
            manufacturer_url: data.manufacturer_url,
            model_name: data.model_name.into_iter().map(|it| it.into()).collect(),
            model_url: data.model_url,
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<EndpointMetadata> for DeviceMetadata {
    fn into(self) -> EndpointMetadata {
        EndpointMetadata {
            friendly_name: self.friendly_name.into_iter().map(|it| it.into()).collect(),
            firmware_version: self.firmware_version,
            hardware_version: self.hardware_version,
            software_version: self.software_version,
            serial_number: self.serial_number,
            manufacturer: self.manufacturer.into_iter().map(|it| it.into()).collect(),
            manufacturer_url: self.manufacturer_url,
            model_name: self.model_name.into_iter().map(|it| it.into()).collect(),
            model_url: self.model_url,
        }
    }
}

#[derive(Clone, Debug, Default, serde::Serialize)]
pub struct LocalizedString {
    pub value: String,
    ///
    /// The language identified by a language code conforming to RFC3066 (<https://www.ietf.org/rfc/rfc3066.txt>).
    ///
    pub locale: String,
}

impl From<protosdc_proto::common::LocalizedString> for LocalizedString {
    fn from(data: protosdc_proto::common::LocalizedString) -> Self {
        Self {
            value: data.value,
            locale: data.locale,
        }
    }
}

#[allow(clippy::from_over_into)]
impl Into<protosdc_proto::common::LocalizedString> for LocalizedString {
    fn into(self) -> protosdc_proto::common::LocalizedString {
        protosdc_proto::common::LocalizedString {
            value: self.value,
            locale: self.locale,
        }
    }
}

impl From<LocalizedStringType> for LocalizedString {
    fn from(value: LocalizedStringType) -> Self {
        Self {
            value: value.value,
            locale: value.lang.unwrap_or_default(),
        }
    }
}

#[derive(Clone, Debug, serde::Serialize, PartialEq)]
pub enum Protocol {
    MDPWS,
    PROTOSDC,
}
