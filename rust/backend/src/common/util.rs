use std::fmt::Debug;

use log::error;
use tokio::sync::{broadcast, mpsc, oneshot};

pub async fn send_log_error<T: Into<U>, U: Debug>(event: T, channel: &mpsc::Sender<U>) {
    if let Err(err) = channel.send(event.into()).await {
        error!("Error sending backend event: {:?}", err)
    };
}

pub fn oneshot_log_error<T: Into<U>, U: Debug>(event: T, channel: oneshot::Sender<U>) {
    if let Err(err) = channel.send(event.into()) {
        error!("Error sending backend event: {:?}", err)
    };
}

pub async fn broadcast_event_log_error<T: Into<U>, U: Debug>(
    event: T,
    channel: &broadcast::Sender<U>,
) {
    if let Err(err) = channel.send(event.into()) {
        error!("Error sending backend event: {:?}", err)
    };
}
