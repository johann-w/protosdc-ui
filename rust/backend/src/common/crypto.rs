use protosdc::common::crypto::CryptoConfig;
use thiserror::Error;

use api::org::protosdc::ui::model::frontend::Crypto;

#[derive(Clone, Debug, Default)]
pub(crate) struct CertificateConfig {
    pub(crate) ca_key: Option<Vec<u8>>,
    pub(crate) public_key: Option<Vec<u8>>,
    pub(crate) private_key: Option<Vec<u8>>,
    pub(crate) enable_tls: bool,
    pub(crate) private_key_password: Option<String>,
}

#[derive(Clone, Debug, Error)]
pub enum CertificateConfigError {
    #[error("No CA key")]
    NoCaKey,
    #[error("Invalid CA key")]
    InvalidCaKeyPath,
    #[error("No public key")]
    NoPublicKey,
    #[error("Invalid public key")]
    InvalidPublicKeyPath,
    #[error("No private key")]
    NoPrivateKey,
    #[error("Invalid private key")]
    InvalidPrivateKeyPath,
}

// contains crypto keys as strings, _not_ as paths to files
#[derive(Clone, Debug, Default, serde::Serialize)]
pub struct DeviceCertificateConfig {
    pub ca_key: Vec<u8>,
    pub public_key: Vec<u8>,
    pub private_key: Vec<u8>,
    pub private_key_password: Option<String>,
}

impl Into<CryptoConfig> for DeviceCertificateConfig {
    fn into(self) -> CryptoConfig {
        CryptoConfig::new_from_memory(
            self.private_key,
            self.public_key,
            self.ca_key,
            self.private_key_password,
        )
    }
}

impl From<Crypto> for DeviceCertificateConfig {
    fn from(value: Crypto) -> DeviceCertificateConfig {
        DeviceCertificateConfig {
            ca_key: value.ca_cert,
            public_key: value.participant_public,
            private_key: value.participant_private,
            private_key_password: value.participant_private_password,
        }
    }
}
