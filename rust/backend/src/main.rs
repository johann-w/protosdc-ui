// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::fs;
use std::io::Write;
use std::pin::Pin;

use futures::{stream, Stream};
use log::{debug, error, info, LevelFilter};
use tauri::{App, Runtime};
use tokio::sync::{broadcast, oneshot};
use tokio::sync::mpsc;
use tokio_stream::StreamExt as TokioStreamExt;
use tokio_stream::wrappers::BroadcastStream;
use tonic::{Request, Response, Status};
use tonic::transport::Server;

use api::org::protosdc::ui::model::backend::backend_service_server::{
    BackendService, BackendServiceServer,
};
use api::org::protosdc::ui::model::backend::discovery;
use api::org::protosdc::ui::model::backend::mdib::{
    MdibUpdateEvent as ApiMdibUpdateEvent, MdibUpdateSubscribe,
};
use api::org::protosdc::ui::model::backend::remote_device::RemoteDeviceEvent as ApiRemoteDeviceEvent;
use api::org::protosdc::ui::model::frontend::{
    ConnectProviderRequest, ConnectProviderResponse, DisconnectProviderRequest,
    DisconnectProviderResponse, GetRawMdibRequest, GetRawMdibResponse, StartDiscoveryRequest,
    StartDiscoveryResponse,
};
use api::org::protosdc::ui::model::frontend::frontend_service_server::{
    FrontendService, FrontendServiceServer,
};

use crate::consumer::device_bookkeeper::DeviceBookkeeper;
use crate::consumer::discovery::run_discovery_loop;
use crate::consumer::events::{
    BookkeeperEvent, FrontendCommand, FrontendDiscoveryEvent, RemoteDeviceEvent,
};
use crate::consumer::remote_device_container::remote_device_loop;

pub mod consumer {
    pub mod device_bookkeeper;
    pub mod discovery;
    pub mod events;
    pub mod remote_device_container;
}

pub mod common {
    pub mod crypto;
    pub mod metadata;
    pub mod util;
}

struct FrontendServicerImpl {
    discovery_tx: mpsc::Sender<FrontendDiscoveryEvent>,
    connect_tx: mpsc::Sender<FrontendCommand>,
}

struct BackendServicerImpl {
    bookkeeper_tx: broadcast::Sender<BookkeeperEvent>,
    connect_tx: broadcast::Sender<RemoteDeviceEvent>,
    frontend_connect_tx: mpsc::Sender<FrontendCommand>,
}

#[tonic::async_trait]
impl FrontendService for FrontendServicerImpl {
    async fn start_discovery(
        &self,
        request: tonic::Request<StartDiscoveryRequest>,
    ) -> Result<Response<StartDiscoveryResponse>, Status> {
        let inner = request.into_inner();
        debug!("start_discovery on adapter {}", inner.adapter_ip);

        self.discovery_tx
            .send(FrontendDiscoveryEvent::StartDiscovery {
                adapter_ip: inner.adapter_ip,
            })
            .await
            .map_err(|e| Status::aborted(e.to_string()))?;
        Ok(Response::new(StartDiscoveryResponse {}))
    }

    async fn connect_provider(
        &self,
        request: tonic::Request<ConnectProviderRequest>,
    ) -> Result<Response<ConnectProviderResponse>, Status> {
        let inner = request.into_inner();
        debug!("connect_provider for device_id {}", inner.device_id);

        self.connect_tx
            .send(FrontendCommand::Connect {
                device_id: inner.device_id,
                adapter_ip: inner.adapter_ip,
                crypto: inner.crypto.map(|it| it.into()),
            })
            .await
            .map_err(|e| Status::aborted(e.to_string()))?;

        Ok(Response::new(ConnectProviderResponse {}))
    }

    async fn disconnect_provider(
        &self,
        request: Request<DisconnectProviderRequest>,
    ) -> Result<Response<DisconnectProviderResponse>, Status> {
        let inner = request.into_inner();
        debug!("disconnect_provider for device_id {}", inner.device_id);

        self.connect_tx
            .send(FrontendCommand::Disconnect {
                device_id: inner.device_id,
            })
            .await
            .map_err(|e| Status::aborted(e.to_string()))?;

        Ok(Response::new(DisconnectProviderResponse {}))
    }

    async fn get_raw_mdib(
        &self,
        request: Request<GetRawMdibRequest>,
    ) -> Result<Response<GetRawMdibResponse>, Status> {
        let inner = request.into_inner();
        debug!("get_raw_mdib for device_id {}", inner.device_id);

        let (tx, rx) = oneshot::channel();
        self.connect_tx
            .send(FrontendCommand::GetRawMdib {
                device_id: inner.device_id,
                response: tx,
            })
            .await
            .map_err(|e| Status::aborted(e.to_string()))?;

        let raw_mdib = rx
            .await
            .map_err(|err| Status::unknown(format!("{:?}", err)))?;

        Ok(Response::new(raw_mdib.into()))
    }
}

#[tonic::async_trait]
impl BackendService for BackendServicerImpl {
    type BookkeeperUpdatesStream = Pin<
        Box<dyn Stream<Item = Result<discovery::BookkeeperEvent, Status>> + Send + Sync + 'static>,
    >;

    async fn bookkeeper_updates(
        &self,
        _request: tonic::Request<()>,
    ) -> Result<Response<Self::BookkeeperUpdatesStream>, Status> {
        let (tx, rx) = oneshot::channel();
        self.frontend_connect_tx
            .send(FrontendCommand::SubscribeBookkeeper { response: tx })
            .await
            .map_err(|err| Status::unknown(format!("{:?}", err)))?;

        let (initial_devices, receiver) = rx
            .await
            .map_err(|err| Status::unknown(format!("{:?}", err)))?;
        let initial_stream = stream::iter(initial_devices.into_iter().map(|it| Ok(it)));

        let response_stream =
            initial_stream
                .chain(BroadcastStream::new(receiver))
                .map(move |result| match result {
                    Ok(x) => Ok(x.into()),
                    Err(err) => Err(Status::unknown(err.to_string())),
                });

        Ok(Response::new(
            Box::pin(response_stream) as Self::BookkeeperUpdatesStream
        ))
    }

    type ConnectUpdatesStream =
        Pin<Box<dyn Stream<Item = Result<ApiRemoteDeviceEvent, Status>> + Send + Sync + 'static>>;

    async fn connect_updates(
        &self,
        _request: tonic::Request<()>,
    ) -> Result<Response<Self::ConnectUpdatesStream>, Status> {
        let response_stream =
            BroadcastStream::new(self.connect_tx.subscribe()).map(|result| match result {
                Ok(x) => Ok(x.into()),
                Err(err) => Err(Status::unknown(err.to_string())),
            });

        Ok(Response::new(
            Box::pin(response_stream) as Self::ConnectUpdatesStream
        ))
    }

    type MdibUpdatesStream =
        Pin<Box<dyn Stream<Item = Result<ApiMdibUpdateEvent, Status>> + Send + Sync + 'static>>;

    async fn mdib_updates(
        &self,
        request: tonic::Request<MdibUpdateSubscribe>,
    ) -> Result<Response<Self::MdibUpdatesStream>, Status> {
        let inner = request.into_inner();
        let device_id = inner.device_id;
        info!("mdib update subscription for {}", device_id);

        let (tx, rx) = oneshot::channel();
        self.frontend_connect_tx
            .send(FrontendCommand::SubscribeDevice {
                device_id: device_id.clone(),
                response: tx,
            })
            .await
            .map_err(|err| {
                Status::cancelled(format!(
                    "Processing stopped, this likely means that the device id was not known {:?}",
                    err
                ))
            })?;

        let (initial_mdib, receiver) = rx
            .await
            .map_err(|err| Status::unknown(format!("{:?}", err)))?;

        let initial_stream = stream::iter(initial_mdib.into_iter().map(|it| Ok(it)));
        let update_stream = BroadcastStream::new(receiver);
        let combined_stream = initial_stream.chain(update_stream);

        let response_stream = combined_stream.map(|it| match it {
            Ok(x) => Ok(x.into()),
            Err(err) => Err(Status::unknown(err.to_string())),
        });

        Ok(Response::new(
            Box::pin(response_stream) as Self::MdibUpdatesStream
        ))
    }
}

fn init_logger() {
    env_logger::Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{}:{} {} [{}] - {}",
                record.module_path().unwrap_or("unknown"),
                record.line().unwrap_or(0),
                chrono::Local::now().format("%Y-%m-%dT%H:%M:%S"),
                record.level(),
                record.args()
            )
        })
        .filter_level(LevelFilter::Debug)
        .init();
}

#[derive(serde::Deserialize)]
struct Config {
    general: General,
}

#[derive(serde::Deserialize)]
struct General {
    grpc_web_address: String,
}

fn load_config<R: Runtime>(app: &mut App<R>) -> Config {
    let resource_path = app
        .path_resolver()
        .resolve_resource("../../rust_config.toml")
        .expect("failed to resolve resource");

    info!("Loading config {}", resource_path.to_string_lossy());

    let contents =
        fs::read_to_string(resource_path).expect("Should have been able to read the file");

    let config: Config = toml::from_str(&contents).expect("Could not load config");
    config
}

fn main() {
    init_logger();

    // events from frontend to backend
    let (
        frontend_to_backend_discovery_event_input_tx,
        frontend_to_backend_discovery_event_input_rx,
    ) = mpsc::channel(1);

    // events from frontend to bookkeeper
    let (
        frontend_to_bookkeeper_connect_event_input_tx,
        frontend_to_bookkeeper_connect_event_input_rx,
    ) = mpsc::channel(1);

    // events from bookkeeper to backend
    let (bookkeeper_to_backend_connect_event_tx, bookkeeper_to_backend_connect_event_rx) =
        mpsc::channel(1);

    // events from backend to bookkeeper
    let (
        backend_to_bookkeeper_remote_device_event_tx,
        backend_to_bookkeeper_remote_device_event_rx,
    ) = mpsc::channel(1);

    let (backend_to_bookkeeper_discovery_event_tx, backend_to_bookkeeper_discovery_event_rx) =
        mpsc::channel(1);

    // events from bookkeeper to frontend
    let (
        bookkeeper_to_frontend_remote_device_event_tx,
        bookkeeper_to_frontend_remote_device_event_rx,
    ) = broadcast::channel(1);

    // events from backend to frontend

    let bookkeeper = DeviceBookkeeper::new();
    let bookkeeper_sender = bookkeeper.sender();

    tauri::Builder::default()
        .setup(move |app| {
            let config = load_config(app);

            tauri::async_runtime::spawn({
                let task_bookkeeper_to_frontend_remote_device_event_tx =
                    bookkeeper_to_frontend_remote_device_event_tx.clone();
                async move {
                    bookkeeper
                        .run(
                            frontend_to_bookkeeper_connect_event_input_rx,
                            backend_to_bookkeeper_remote_device_event_rx,
                            backend_to_bookkeeper_discovery_event_rx,
                            bookkeeper_to_backend_connect_event_tx,
                            task_bookkeeper_to_frontend_remote_device_event_tx,
                        )
                        .await
                }
            });
            tauri::async_runtime::spawn({
                async move {
                    run_discovery_loop(
                        frontend_to_backend_discovery_event_input_rx,
                        backend_to_bookkeeper_discovery_event_tx,
                    )
                    .await
                }
            });
            tauri::async_runtime::spawn({
                async move {
                    remote_device_loop(
                        bookkeeper_to_backend_connect_event_rx,
                        backend_to_bookkeeper_remote_device_event_tx,
                    )
                    .await
                }
            });
            tauri::async_runtime::spawn({
                let addr = config.general.grpc_web_address.parse().unwrap();
                let frontend_service = FrontendServiceServer::new(FrontendServicerImpl {
                    discovery_tx: frontend_to_backend_discovery_event_input_tx,
                    connect_tx: frontend_to_bookkeeper_connect_event_input_tx.clone(),
                });
                let backend_service = BackendServiceServer::new(BackendServicerImpl {
                    bookkeeper_tx: bookkeeper_sender,
                    connect_tx: bookkeeper_to_frontend_remote_device_event_tx,
                    frontend_connect_tx: frontend_to_bookkeeper_connect_event_input_tx,
                });

                async move {
                    debug!("Starting grpc-web server on {}", addr);
                    let result = Server::builder()
                        .accept_http1(true)
                        .add_service(tonic_web::enable(frontend_service))
                        .add_service(tonic_web::enable(backend_service))
                        .serve(addr)
                        .await;

                    info!("Stopped grpc-web server on {}", addr);

                    if let Err(err) = result {
                        error!("{:?}", err)
                    }
                }
            });

            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
