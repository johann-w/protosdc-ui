use std::collections::HashMap;
use std::net::IpAddr;
use std::sync::Arc;

use futures::FutureExt;
use itertools::Itertools;
use log::{debug, error};
use thiserror::Error;
use tokio::sync::{broadcast, mpsc, Mutex};

use api::org::protosdc::ui::model::backend::discovery::BookkeeperDevice;
use api::org::protosdc::ui::model::common::ConnectionState;

use crate::common::crypto::DeviceCertificateConfig;
use crate::common::util::{broadcast_event_log_error, oneshot_log_error, send_log_error};
use crate::consumer::discovery::DiscoveredDevice;
use crate::consumer::events::{
    BookkeeperEvent, DiscoveryStateEvent, FrontendCommand, RemoteDeviceEvent,
};
use crate::consumer::remote_device_container::RemoteDeviceMessage;

#[derive(Debug, Error)]
enum BookkeeperError {
    #[error("The device id {device_id} is not known to the bookkeeper")]
    UnknownDeviceId { device_id: String },

    #[error("The provided address could not be converted to IpAddr")]
    AdapterNotIp,
}

#[derive(Clone, Debug, PartialEq)]
pub enum DeviceState {
    Discovered,
    Connecting,
    Connected,
}

impl Into<ConnectionState> for DeviceState {
    fn into(self) -> ConnectionState {
        match self {
            DeviceState::Discovered => ConnectionState::Disconnected,
            DeviceState::Connecting => ConnectionState::Connecting,
            DeviceState::Connected => ConnectionState::Connected,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Entry {
    device_id: String,
    state: DeviceState,
    discovery: DiscoveredDevice,
}

impl Into<BookkeeperDevice> for Entry {
    fn into(self) -> BookkeeperDevice {
        self.discovery.to_api(self.device_id, self.state)
    }
}

impl Into<BookkeeperEvent> for Entry {
    fn into(self) -> BookkeeperEvent {
        BookkeeperEvent::DeviceDiscoveryUpdate {
            device_id: self.device_id,
            discovery: self.discovery,
        }
    }
}

type Books = HashMap<String, Entry>;
type LockedBooks = Arc<Mutex<Books>>;
type BookSender = broadcast::Sender<BookkeeperEvent>;

#[derive(Debug)]
pub struct DeviceBookkeeper {
    books: LockedBooks,
    bookkeeper_tx: broadcast::Sender<BookkeeperEvent>,
}

impl DeviceBookkeeper {
    pub fn new() -> Self {
        let (tx, _rx) = broadcast::channel(1);
        Self {
            books: Arc::new(Mutex::new(HashMap::new())),
            bookkeeper_tx: tx,
        }
    }

    pub fn subscribe(&self) -> broadcast::Receiver<BookkeeperEvent> {
        self.bookkeeper_tx.subscribe()
    }

    pub async fn subscribe_with_state(
        locked_books: &LockedBooks,
        sender: &broadcast::Sender<BookkeeperEvent>,
    ) -> (Vec<BookkeeperEvent>, broadcast::Receiver<BookkeeperEvent>) {
        // locked books ensure state doesn't change
        let books = locked_books.lock().await;
        let subscription = sender.subscribe();

        let devices = Self::get_devices(&books)
            .into_iter()
            .map(|it| it.into())
            .collect_vec();

        (devices, subscription)
    }

    pub fn sender(&self) -> broadcast::Sender<BookkeeperEvent> {
        self.bookkeeper_tx.clone()
    }

    pub async fn run(
        &self,
        mut connect_event_rx: mpsc::Receiver<FrontendCommand>,
        mut remote_device_event_rx: mpsc::Receiver<RemoteDeviceEvent>,
        mut discovery_event_rx: mpsc::Receiver<DiscoveryStateEvent>,

        connect_event_tx: mpsc::Sender<RemoteDeviceMessage>,
        remote_device_event_tx: broadcast::Sender<RemoteDeviceEvent>,
    ) {
        let task_books = self.books.clone();
        let book_sender = self.bookkeeper_tx.clone();

        loop {
            let m: BookkeeperEvent = futures::select! {
                res = discovery_event_rx.recv().fuse() => {
                    match res {
                        None => return,
                        Some(n) => {
                            debug!("{:?}", n);
                            match n {
                                DiscoveryStateEvent::DiscoveryStarted => {
                                    Self::prune_devices(&task_books, &book_sender).await;
                                },
                                DiscoveryStateEvent::DiscoveredDevice(device) => {
                                    Self::register_discovery_device(&task_books, &book_sender, device).await;
                                }
                                DiscoveryStateEvent::DiscoveryFinished => {},
                                DiscoveryStateEvent::DiscoveryFailed { .. } => {},
                            }
                        }
                    };
                    continue
                }
                res = remote_device_event_rx.recv().fuse() => {
                    match res {
                        None => return,
                        Some(n) => {
                            debug!("{:?}", n);
                            match &n {
                                RemoteDeviceEvent::RemoteDeviceConnected { device_id, metadata } => {
                                    Self::connected_device(&task_books, device_id).await;
                                },
                                RemoteDeviceEvent::RemoteDeviceConnectFailed { device_id, reason } => {
                                    Self::disconnect_device(&task_books, device_id).await;
                                },
                                RemoteDeviceEvent::RemoteDeviceDisconnected { device_id } => {
                                    Self::disconnect_device(&task_books, device_id).await;
                                },
                                RemoteDeviceEvent::RemoteDeviceDisconnecting { device_id } => {},
                                RemoteDeviceEvent::RemoteDeviceConnecting { device_id } => {},
                            }
                            broadcast_event_log_error(n, &remote_device_event_tx).await;
                        }
                    }
                    continue
                }
                res = connect_event_rx.recv().fuse() => {
                    match res {
                        None => return,
                        Some(n) => {
                            debug!("{:?}", n);
                            match n {
                                FrontendCommand::Connect { device_id, adapter_ip, crypto } => {
                                    if let Err(err) = Self::connect_device(
                                        &task_books,
                                        &connect_event_tx,
                                        &device_id,
                                        &adapter_ip,
                                        crypto
                                    ).await {
                                        error!("{:?}", err);
                                    };
                                },
                                FrontendCommand::Disconnect { device_id } => {
                                    let msg = RemoteDeviceMessage::Disconnect { device_id };
                                    send_log_error(msg, &connect_event_tx).await;
                                },
                                FrontendCommand::Quit {} => {
                                    // TODO disconnect all
                                    return;
                                },
                                FrontendCommand::GetConnectedDevices { response} => {
                                    let msg = RemoteDeviceMessage::GetConnectedDevices { response };
                                    send_log_error(msg, &connect_event_tx).await;
                                },
                                FrontendCommand::SubscribeBookkeeper { response } => {
                                    let msg = Self::subscribe_with_state(
                                        &task_books,
                                        &book_sender
                                    ).await;
                                    oneshot_log_error(msg, response);
                                }
                                FrontendCommand::SubscribeDevice { device_id, response } => {
                                    let msg = RemoteDeviceMessage::SubscribeDevice { device_id, response };
                                    send_log_error(msg, &connect_event_tx).await;
                                }
                                FrontendCommand::GetRawMdib { device_id, response } => {
                                    let msg = RemoteDeviceMessage::GetRawMdib { device_id, response };
                                    send_log_error(msg, &connect_event_tx).await;
                                }
                            }
                        }
                    };
                    continue
                }
            };
        }
    }

    async fn register_discovery_device(
        locked_books: &LockedBooks,
        sender: &BookSender,
        discovered_device: DiscoveredDevice,
    ) {
        let mut books = locked_books.lock().await;

        // check if there know this device already
        let known_device = books.values_mut().find(|it| {
            it.discovery.endpoint_reference_address == discovered_device.endpoint_reference_address
                && it.discovery.protocol == discovered_device.protocol
        });

        match known_device {
            Some(known) => {
                // check if something changed
                if known.discovery != discovered_device {
                    debug!("Device {} changed, updating", known.device_id);
                    known.discovery = discovered_device.clone();
                    let event = BookkeeperEvent::DeviceDiscoveryUpdate {
                        device_id: known.device_id.clone(),
                        discovery: discovered_device,
                    };
                    broadcast_event_log_error(event, sender).await;
                } else {
                    debug!("Device {} unchanged, ignoring", known.device_id);
                    // ignore, nothing changed
                }
            }
            None => {
                let device_id = uuid::Uuid::new_v4().to_string();

                debug!("Device {} is new, inserting", device_id);

                let entry = Entry {
                    device_id: device_id.clone(),
                    state: DeviceState::Discovered,
                    discovery: discovered_device.clone(),
                };
                books.insert(device_id.clone(), entry);

                let event = BookkeeperEvent::DeviceDiscoveryUpdate {
                    device_id,
                    discovery: discovered_device,
                };
                broadcast_event_log_error(event, sender).await;
            }
        }
    }

    /// Prunes all devices from the bookkeeper that were only discovered.
    /// Used when a new discovery begins, meaning old data becomes state.
    async fn prune_devices(locked_books: &LockedBooks, sender: &BookSender) {
        let mut books = locked_books.lock().await;

        debug!("Books contain {} devices", books.len());

        // find devices to remove
        let to_remove = books
            .iter()
            .filter(|it| it.1.state == DeviceState::Discovered)
            .map(|it| it.0.clone())
            .collect::<Vec<_>>();

        debug!("Pruning {:?}", to_remove);

        for it in &to_remove {
            books.remove(it);
        }

        let msg = BookkeeperEvent::DevicesRemoved {
            device_ids: to_remove,
        };
        broadcast_event_log_error(msg, sender).await;
    }

    async fn change_state(
        locked_books: &LockedBooks,
        device_id: &str,
        state: DeviceState,
    ) -> Result<(), BookkeeperError> {
        let mut books = locked_books.lock().await;

        // verify we know the device
        match books.get_mut(device_id) {
            None => Err(BookkeeperError::UnknownDeviceId {
                device_id: device_id.to_string(),
            }),
            Some(entry) => {
                entry.state = state;
                Ok(())
            }
        }
    }

    fn get_devices(books: &Books) -> Vec<Entry> {
        books
            .iter()
            .map(|(device_id, entry)| entry.clone())
            .collect_vec()
    }

    async fn connect_device(
        locked_books: &LockedBooks,
        connect_event_tx: &mpsc::Sender<RemoteDeviceMessage>,
        device_id: &str,
        adapter: &str,
        crypto: Option<DeviceCertificateConfig>,
    ) -> Result<(), BookkeeperError> {
        let adapter_ip: IpAddr = adapter.parse().map_err(|_| BookkeeperError::AdapterNotIp)?;

        let mut books = locked_books.lock().await;

        // verify we know the device
        match books.get_mut(device_id) {
            None => Err(BookkeeperError::UnknownDeviceId {
                device_id: device_id.to_string(),
            }),
            Some(entry) => {
                entry.state = DeviceState::Connecting;

                let msg = RemoteDeviceMessage::Connect {
                    device_id: device_id.to_string(),
                    device_epr: entry.discovery.endpoint_reference_address.clone(),
                    adapter_ip,
                    protocol: entry.discovery.protocol.clone(),
                    crypto,
                };
                send_log_error(msg, connect_event_tx).await;
                Ok(())
            }
        }
    }

    async fn disconnect_device(
        locked_books: &LockedBooks,
        device_id: &str,
    ) -> Result<(), BookkeeperError> {
        Self::change_state(locked_books, device_id, DeviceState::Discovered).await
    }

    async fn connected_device(
        locked_books: &LockedBooks,
        device_id: &str,
    ) -> Result<(), BookkeeperError> {
        Self::change_state(locked_books, device_id, DeviceState::Connected).await
    }
}

#[cfg(test)]
mod test {

    #[tokio::test]
    async fn implement_me() {
        todo!()
    }
}
