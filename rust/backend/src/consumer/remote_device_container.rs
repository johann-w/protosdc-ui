use std::cmp::Ordering;
use std::collections::HashMap;
use std::net::IpAddr;
use std::sync::Arc;

use biceps::{
    common::storage::mdib_storage::MdibStorageImpl,
    consumer::access::remote_mdib_access::RemoteMdibAccessImpl,
};
use biceps::common::access::mdib_access::MdibAccessEvent;
use biceps::common::mdib_entity::{Entity, MdibEntity};
use biceps::common::mdib_pair::Pair;
use biceps::common::mdib_version::MdibVersion;
use biceps::consumer::access::remote_mdib_access::RemoteMdibAccess;
use bytes::Bytes;
use common::Service;
use dpws::{
    consumer::{sco::DpwsSetServiceHandler, sdc_consumer::SdcRemoteDevice as MdpwsRemoteDevice},
    http::client::reqwest::ReqwestClientAccess,
    soap::{
        dpws::client::consumer::{ConsumerHostedService, ConsumerHostingService},
        soap_client::SoapClientImpl,
    },
};
use dpws::consumer::sdc_consumer::ConsumerConfigBuilder;
use dpws::consumer::sdc_remote_device_connector::{
    ALL_EPISODIC_AND_WAVEFORM_REPORTS, ConnectConfiguration, SdcRemoteDeviceConnectorError,
    SdcRemoteDeviceConnectorImpl,
};
use dpws::discovery::constants::WS_DISCOVERY_PORT;
use dpws::discovery::consumer::discovery_consumer::DpwsDiscoveryConsumerImpl;
use dpws::http::client::common::{HttpClient, HttpClientError};
use dpws::http::client::reqwest::ReqwestHttpClient;
use dpws::http::server::axum::AxumRegistry;
use dpws::soap::dpws::client::consumer::{Consumer, ConsumerImpl};
use dpws::soap::dpws::client::hosted_service::HostedService;
use dpws::soap::dpws::client::hosting_service::HostingService;
use dpws::xml::common::create_header_for_action;
use dpws::xml::parsing::biceps_ext::{BICEPS_ACTION_GET_MDIB, BICEPS_PORT_TYPE_GET};
use dpws::xml::parsing::common::ConstQualifiedName;
use dpws::xml::parsing::soap::envelope::SoapMessage;
use futures::{FutureExt, StreamExt};
use futures::future::Fuse;
use itertools::Itertools;
use log::{debug, error, info};
use network::udp_binding::create_udp_binding;
use protosdc::consumer::grpc_consumer::ConsumerImpl as ProtoConsumerImpl;
use protosdc::consumer::remote_device::SdcRemoteDevice;
use protosdc::consumer::sco::{sco_controller::ScoController, sco_transaction::ScoTransactionImpl};
use protosdc::consumer::sco::sco_controller::ProtoSetServiceHandler;
use protosdc_biceps::biceps::{
    AbstractAlertStateOneOf, AbstractContextStateOneOf, AbstractDeviceComponentStateOneOf,
    AbstractGet, AbstractMetricStateOneOf, AbstractOperationStateOneOf, GetMdib,
};
use protosdc_xml::XmlWriter;
use tokio::{sync::mpsc, task::JoinHandle};
use tokio::sync::{broadcast, Mutex, oneshot};
use tokio::sync::broadcast::error::TryRecvError;
use tokio_stream::wrappers::ReceiverStream;

use api::org::protosdc::ui::model::backend;
use api::org::protosdc::ui::model::backend::mdib::{
    alert_pair, component_pair, context_pair, metric_pair, operation_pair,
};

use crate::common::crypto::DeviceCertificateConfig;
use crate::common::metadata::{DeviceMetadata, Protocol};
use crate::common::util::{broadcast_event_log_error, oneshot_log_error, send_log_error};
use crate::consumer::events::{
    AlertUpdate, AlertUpdateEvent, ComponentUpdate, ComponentUpdateEvent, ContextStateUpdate,
    ContextUpdate, ContextUpdateEvent, ContextUpdateKind, MdibChange, MdibUpdate, MetricUpdate,
    MetricUpdateEvent, OperationUpdate, OperationUpdateEvent, RawMdibResponse, RemoteDeviceEvent,
};

pub type ReqwestSoapClient = SoapClientImpl<ReqwestClientAccess>;

pub type MdpwsSdcRemoteDevice = MdpwsRemoteDevice<
    RemoteMdibAccessImpl<MdibStorageImpl>,
    ConsumerHostingService,
    ConsumerHostedService,
    ReqwestSoapClient,
    ReqwestSoapClient,
    ScoController<DpwsSetServiceHandler<ReqwestSoapClient>>,
    ScoTransactionImpl,
>;

pub type ProtoRemoteDevice = SdcRemoteDevice<
    ProtoConsumerImpl,
    RemoteMdibAccessImpl<MdibStorageImpl>,
    ScoController<ProtoSetServiceHandler>,
    ScoTransactionImpl,
>;

const MDIB_UPDATE_CHANNEL_SIZE: usize = 10;

pub enum RemoteDevice {
    MDPWS(MdpwsSdcRemoteDevice),
    PROTOSDC(ProtoRemoteDevice),
}

#[derive(Clone, Debug, serde::Serialize)]
pub struct RemoteDeviceFrontend {
    device_id: String,
    adapter_ip: String,
    protocol: Protocol,
}

#[derive(Clone, Debug)]
pub struct GetMdibResponse {
    mdib_version: MdibVersion,
    pairs: Vec<Pair>,
}

#[derive(Debug)]
pub enum RemoteDeviceMessage {
    Connect {
        device_id: String,
        device_epr: String,
        adapter_ip: IpAddr,
        protocol: Protocol,
        crypto: Option<DeviceCertificateConfig>,
    },
    Disconnect {
        device_id: String,
    },
    Quit,
    GetConnectedDevices {
        response: oneshot::Sender<Vec<RemoteDeviceFrontend>>,
    },
    SubscribeDevice {
        device_id: String,
        response: oneshot::Sender<(Vec<MdibUpdate>, broadcast::Receiver<MdibUpdate>)>,
    },
    GetRawMdib {
        device_id: String,
        response: oneshot::Sender<RawMdibResponse>,
    },
}

#[derive(Clone, Debug, serde::Serialize)]
pub struct Metadata {
    pub device_metadata: DeviceMetadata,
}

#[allow(dead_code)]
pub struct RemoteDeviceContainer {
    device_id: String,
    remote_device: RemoteDevice,
    metadata: Metadata,
    subscription_task: JoinHandle<()>,
    adapter_ip: String,
    broadcast_sender: broadcast::Sender<MdibUpdate>,
    // metadata_sender: mpsc::UnboundedSender<GetMetadataResponse>,
    // disconnect_sender: oneshot::Sender<()>,
}

struct MdpwsConnectContainer {
    device_id: String,
    remote_device: MdpwsSdcRemoteDevice,
    adapter_ip: String,
}

#[derive(Clone, Debug)]
pub enum PairHolder {
    Metric(backend::mdib::MetricPair),
    Alert(backend::mdib::AlertPair),
    Component(backend::mdib::ComponentPair),
    Context(backend::mdib::ContextPair),
    Operation(backend::mdib::OperationPair),
}

fn entity_to_pair_holder(mdib_entity: MdibEntity) -> PairHolder {
    match mdib_entity.entity {
        Entity::StringMetric(ent) => PairHolder::Metric(backend::mdib::MetricPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(metric_pair::Pair::StringMetricPair(
                backend::mdib::StringMetricPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::EnumStringMetric(ent) => PairHolder::Metric(backend::mdib::MetricPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(metric_pair::Pair::EnumStringMetricPair(
                backend::mdib::EnumStringMetricPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::NumericMetric(ent) => PairHolder::Metric(backend::mdib::MetricPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(metric_pair::Pair::NumericMetricPair(
                backend::mdib::NumericMetricPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::RealTimeSampleArrayMetric(ent) => PairHolder::Metric(backend::mdib::MetricPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(metric_pair::Pair::RealTimeSampleArrayMetricPair(
                backend::mdib::RealTimeSampleArrayMetricPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::DistributionSampleArrayMetric(ent) => {
            PairHolder::Metric(backend::mdib::MetricPair {
                pair_info: Some(backend::mdib::Pair {
                    parent_handle: Some(ent.parent),
                }),
                pair: Some(metric_pair::Pair::DistributionSampleArrayMetricPair(
                    backend::mdib::DistributionSampleArrayMetricPair {
                        descriptor: Some(ent.pair.descriptor.into()),
                        state: Some(ent.pair.state.into()),
                    },
                )),
            })
        }
        Entity::AlertCondition(ent) => PairHolder::Alert(backend::mdib::AlertPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(alert_pair::Pair::AlertConditionPair(
                backend::mdib::AlertConditionPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::LimitAlertCondition(ent) => PairHolder::Alert(backend::mdib::AlertPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(alert_pair::Pair::LimitAlertConditionPair(
                backend::mdib::LimitAlertConditionPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::AlertSignal(ent) => PairHolder::Alert(backend::mdib::AlertPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(alert_pair::Pair::AlertSignalPair(
                backend::mdib::AlertSignalPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::AlertSystem(ent) => PairHolder::Alert(backend::mdib::AlertPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(alert_pair::Pair::AlertSystemPair(
                backend::mdib::AlertSystemPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::Mds(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: None,
            }),
            pair: Some(component_pair::Pair::MdsPair(backend::mdib::MdsPair {
                descriptor: Some(ent.pair.descriptor.into()),
                state: Some(ent.pair.state.into()),
            })),
        }),
        Entity::Vmd(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::VmdPair(backend::mdib::VmdPair {
                descriptor: Some(ent.pair.descriptor.into()),
                state: Some(ent.pair.state.into()),
            })),
        }),
        Entity::Channel(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::ChannelPair(
                backend::mdib::ChannelPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::Sco(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::ScoPair(backend::mdib::ScoPair {
                descriptor: Some(ent.pair.descriptor.into()),
                state: Some(ent.pair.state.into()),
            })),
        }),
        Entity::SystemContext(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::SystemContextPair(
                backend::mdib::SystemContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::Battery(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::BatteryPair(
                backend::mdib::BatteryPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::Clock(ent) => PairHolder::Component(backend::mdib::ComponentPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(component_pair::Pair::ClockPair(backend::mdib::ClockPair {
                descriptor: Some(ent.pair.descriptor.into()),
                state: Some(ent.pair.state.into()),
            })),
        }),
        Entity::SetValueOperation(ent) => PairHolder::Operation(backend::mdib::OperationPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(operation_pair::Pair::SetValueOperationPair(
                backend::mdib::SetValueOperationPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::SetStringOperation(ent) => PairHolder::Operation(backend::mdib::OperationPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(operation_pair::Pair::SetStringOperationPair(
                backend::mdib::SetStringOperationPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),
        Entity::SetAlertStateOperation(ent) => {
            PairHolder::Operation(backend::mdib::OperationPair {
                pair_info: Some(backend::mdib::Pair {
                    parent_handle: Some(ent.parent),
                }),
                pair: Some(operation_pair::Pair::SetAlertStateOperationPair(
                    backend::mdib::SetAlertStateOperationPair {
                        descriptor: Some(ent.pair.descriptor.into()),
                        state: Some(ent.pair.state.into()),
                    },
                )),
            })
        }
        Entity::SetMetricStateOperation(ent) => {
            PairHolder::Operation(backend::mdib::OperationPair {
                pair_info: Some(backend::mdib::Pair {
                    parent_handle: Some(ent.parent),
                }),
                pair: Some(operation_pair::Pair::SetMetricStateOperationPair(
                    backend::mdib::SetMetricStateOperationPair {
                        descriptor: Some(ent.pair.descriptor.into()),
                        state: Some(ent.pair.state.into()),
                    },
                )),
            })
        }
        Entity::SetComponentStateOperation(ent) => {
            PairHolder::Operation(backend::mdib::OperationPair {
                pair_info: Some(backend::mdib::Pair {
                    parent_handle: Some(ent.parent),
                }),
                pair: Some(operation_pair::Pair::SetComponentStateOperationPair(
                    backend::mdib::SetComponentStateOperationPair {
                        descriptor: Some(ent.pair.descriptor.into()),
                        state: Some(ent.pair.state.into()),
                    },
                )),
            })
        }
        Entity::SetContextStateOperation(ent) => {
            PairHolder::Operation(backend::mdib::OperationPair {
                pair_info: Some(backend::mdib::Pair {
                    parent_handle: Some(ent.parent),
                }),
                pair: Some(operation_pair::Pair::SetContextStateOperationPair(
                    backend::mdib::SetContextStateOperationPair {
                        descriptor: Some(ent.pair.descriptor.into()),
                        state: Some(ent.pair.state.into()),
                    },
                )),
            })
        }
        Entity::ActivateOperation(ent) => PairHolder::Operation(backend::mdib::OperationPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(operation_pair::Pair::ActivateOperationPair(
                backend::mdib::ActivateOperationPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    state: Some(ent.pair.state.into()),
                },
            )),
        }),

        Entity::PatientContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::PatientContextPair(
                backend::mdib::PatientContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
        Entity::LocationContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::LocationContextPair(
                backend::mdib::LocationContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
        Entity::EnsembleContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::EnsembleContextPair(
                backend::mdib::EnsembleContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
        Entity::WorkflowContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::WorkflowContextPair(
                backend::mdib::WorkflowContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
        Entity::MeansContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::MeansContextPair(
                backend::mdib::MeansContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
        Entity::OperatorContext(ent) => PairHolder::Context(backend::mdib::ContextPair {
            pair_info: Some(backend::mdib::Pair {
                parent_handle: Some(ent.parent),
            }),
            pair: Some(context_pair::Pair::OperatorContextPair(
                backend::mdib::OperatorContextPair {
                    descriptor: Some(ent.pair.descriptor.into()),
                    states: ent.pair.states.into_iter().map(|it| it.into()).collect(),
                },
            )),
        }),
    }
}

async fn process_connect_mdpws_result(
    fut: Result<MdpwsConnectContainer, (String, anyhow::Error)>,
    connected_devices: &Arc<Mutex<HashMap<String, RemoteDeviceContainer>>>,
    channel: &mpsc::Sender<RemoteDeviceEvent>,
) {
    match fut {
        Ok(connect_container) => {
            // subscribe to the changes before generating the initial changeset,
            // ensures we have no gaps
            let receiver = connect_container
                .remote_device
                .remote_mdib
                .subscribe()
                .await;

            let (mdib_channel, _) = broadcast::channel(MDIB_UPDATE_CHANNEL_SIZE);

            let task = tokio::spawn({
                let task_channel = mdib_channel.clone();
                let task_device_id = connect_container.device_id.clone();
                async move {
                    let mut event_stream = ReceiverStream::new(receiver);
                    while let Some(item) = event_stream.next().await {
                        match item {
                            MdibAccessEvent::AlertStateModification {
                                states,
                                mdib_version,
                            } => {
                                let alert_updates = states
                                    .into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .filter_map(|it| match it {
                                        AbstractAlertStateOneOf::AbstractAlertState(_) => None,
                                        AbstractAlertStateOneOf::AlertConditionState(state) => {
                                            Some(AlertUpdate::AlertCondition { state })
                                        }
                                        AbstractAlertStateOneOf::AlertSignalState(state) => {
                                            Some(AlertUpdate::AlertSignal { state })
                                        }
                                        AbstractAlertStateOneOf::AlertSystemState(state) => {
                                            Some(AlertUpdate::AlertSystem { state })
                                        }
                                        AbstractAlertStateOneOf::LimitAlertConditionState(
                                            state,
                                        ) => Some(AlertUpdate::LimitAlertCondition { state }),
                                    })
                                    .collect::<Vec<_>>();

                                debug!("Updating {} alerts", alert_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version,
                                    updates: vec![MdibChange::AlertUpdates(AlertUpdateEvent {
                                        alert_update: alert_updates,
                                    })],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::ComponentStateModification {
                                states,
                                mdib_version,
                            } => {
                                let component_updates = states.into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .filter_map(|it| match it {
                                        AbstractDeviceComponentStateOneOf::AbstractDeviceComponentState(_) => None,
                                        AbstractDeviceComponentStateOneOf::AbstractComplexDeviceComponentState(_) => None,
                                        AbstractDeviceComponentStateOneOf::BatteryState(state) => Some(ComponentUpdate::Battery { state }),
                                        AbstractDeviceComponentStateOneOf::ChannelState(state) => Some(ComponentUpdate::Channel { state }),
                                        AbstractDeviceComponentStateOneOf::ClockState(state) => Some(ComponentUpdate::Clock { state }),
                                        AbstractDeviceComponentStateOneOf::MdsState(state) => Some(ComponentUpdate::Mds { state }),
                                        AbstractDeviceComponentStateOneOf::ScoState(state) => Some(ComponentUpdate::Sco { state }),
                                        AbstractDeviceComponentStateOneOf::SystemContextState(state) => Some(ComponentUpdate::SystemContext { state }),
                                        AbstractDeviceComponentStateOneOf::VmdState(state) => Some(ComponentUpdate::Vmd { state }),
                                    })
                                    .collect::<Vec<_>>();

                                debug!("Updating {} components", component_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version,
                                    updates: vec![MdibChange::ComponentUpdate(
                                        ComponentUpdateEvent {
                                            component_update: component_updates,
                                        },
                                    )],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::ContextStateModification {
                                removed_context_states,
                                updated_context_states,
                                mdib_version,
                            } => {
                                let updated = updated_context_states
                                    .into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .map(|it| (ContextUpdateKind::InsertUpdate, it));

                                let context_updates = removed_context_states
                                    .into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .map(|it| (ContextUpdateKind::Delete, it))
                                    .chain(updated)
                                    .filter_map(|(kind, state)| match state {
                                        AbstractContextStateOneOf::AbstractContextState(_) => None,
                                        AbstractContextStateOneOf::EnsembleContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Ensemble { state },
                                            })
                                        }
                                        AbstractContextStateOneOf::LocationContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Location { state },
                                            })
                                        }
                                        AbstractContextStateOneOf::MeansContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Means { state },
                                            })
                                        }
                                        AbstractContextStateOneOf::OperatorContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Operator { state },
                                            })
                                        }
                                        AbstractContextStateOneOf::PatientContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Patient { state },
                                            })
                                        }
                                        AbstractContextStateOneOf::WorkflowContextState(state) => {
                                            Some(ContextUpdate {
                                                kind,
                                                update: ContextStateUpdate::Workflow { state },
                                            })
                                        }
                                    })
                                    .collect_vec();

                                debug!("Updating {} contexts", context_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version,
                                    updates: vec![MdibChange::ContextUpdate(ContextUpdateEvent {
                                        context_update: context_updates,
                                    })],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::MetricStateModification {
                                states,
                                mdib_version,
                            } => {
                                let metric_updates = states
                                    .into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .filter_map(|it|
                                        match it {
                                            AbstractMetricStateOneOf::NumericMetricState(metric) => Some(MetricUpdate::NumericMetric {
                                                state: metric,
                                            }),
                                            AbstractMetricStateOneOf::StringMetricState(metric) => Some(MetricUpdate::StringMetric {
                                                state: metric,
                                            }),
                                            AbstractMetricStateOneOf::EnumStringMetricState(metric) => Some(MetricUpdate::EnumStringMetric {
                                                state: metric,
                                            }),
                                            AbstractMetricStateOneOf::RealTimeSampleArrayMetricState(metric) => Some(MetricUpdate::RealTimeSampleArrayMetric {
                                                state: metric,
                                            }),
                                            _ => None
                                        }
                                    )
                                    .collect::<Vec<_>>();

                                debug!("Updating {} metrics", metric_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version,
                                    updates: vec![MdibChange::MetricUpdate(MetricUpdateEvent {
                                        metric_update: metric_updates,
                                    })],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::OperationStateModification {
                                states,
                                mdib_version,
                            } => {
                                let operation_updates = states
                                    .into_iter()
                                    .flat_map(|(_mds, states)| states.into_iter())
                                    .filter_map(|it|
                                        match it {
                                            AbstractOperationStateOneOf::AbstractOperationState(_) => None,
                                            AbstractOperationStateOneOf::ActivateOperationState(state) => Some(OperationUpdate::ActivateOperation { state }),
                                            AbstractOperationStateOneOf::SetAlertStateOperationState(state) => Some(OperationUpdate::SetAlertStateOperation { state }),
                                            AbstractOperationStateOneOf::SetComponentStateOperationState(state) => Some(OperationUpdate::SetComponentStateOperation { state }),
                                            AbstractOperationStateOneOf::SetContextStateOperationState(state) => Some(OperationUpdate::SetContextStateOperation { state }),
                                            AbstractOperationStateOneOf::SetMetricStateOperationState(state) => Some(OperationUpdate::SetMetricStateOperation { state }),
                                            AbstractOperationStateOneOf::SetStringOperationState(state) => Some(OperationUpdate::SetStringOperation { state }),
                                            AbstractOperationStateOneOf::SetValueOperationState(state) => Some(OperationUpdate::SetValueOperation { state }),
                                        }
                                    )
                                    .collect::<Vec<_>>();

                                debug!("Updating {} operations", operation_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version,
                                    updates: vec![MdibChange::OperationUpdate(
                                        OperationUpdateEvent {
                                            operation_update: operation_updates,
                                        },
                                    )],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::WaveformModification {
                                states,
                                mdib_version,
                            } => {
                                let metric_updates = states
                                    .into_iter()
                                    .map(|state| MetricUpdate::RealTimeSampleArrayMetric {
                                        state: state,
                                    })
                                    .collect::<Vec<_>>();

                                debug!("Updating {} waveforms", metric_updates.len());

                                let evt = MdibUpdate {
                                    device_id: task_device_id.clone(),
                                    mdib_version: mdib_version,
                                    updates: vec![MdibChange::MetricUpdate(MetricUpdateEvent {
                                        metric_update: metric_updates,
                                    })],
                                };

                                broadcast_event_log_error(evt, &task_channel).await;
                            }
                            MdibAccessEvent::DescriptionModification { .. } => {}
                        }
                    }
                }
            });

            let this_device = connect_container
                .remote_device
                .hosting_service
                .lock()
                .await
                .this_device()
                .cloned();
            let this_model = connect_container
                .remote_device
                .hosting_service
                .lock()
                .await
                .this_model()
                .cloned();

            let device_metadata: DeviceMetadata = (this_model, this_device).into();
            let metadata = Metadata { device_metadata };
            let remote_device_container = RemoteDeviceContainer {
                device_id: connect_container.device_id.clone(),
                adapter_ip: connect_container.adapter_ip.clone(),
                remote_device: RemoteDevice::MDPWS(connect_container.remote_device),
                metadata: metadata.clone(),
                subscription_task: task,
                broadcast_sender: mdib_channel,
            };

            connected_devices
                .lock()
                .await
                .insert(connect_container.device_id.clone(), remote_device_container);
            let event = RemoteDeviceEvent::RemoteDeviceConnected {
                device_id: connect_container.device_id,
                metadata,
            };
            send_log_error(event, channel).await;
        }
        Err((device_id, err)) => {
            error!("{:?}", err);
            let event = RemoteDeviceEvent::RemoteDeviceConnectFailed {
                device_id,
                reason: format!("{:?}", err),
            };
            send_log_error(event, channel).await;
        }
    }
}

pub async fn remote_device_loop(
    mut r: mpsc::Receiver<RemoteDeviceMessage>,
    channel: mpsc::Sender<RemoteDeviceEvent>,
) -> anyhow::Result<()> {
    let connected_devices: Arc<Mutex<HashMap<String, RemoteDeviceContainer>>> =
        Arc::new(Mutex::new(HashMap::new()));

    let connect_mdpws_device_future = Fuse::terminated();
    let disconnect_remote_device_future = Fuse::terminated();
    futures::pin_mut!(connect_mdpws_device_future, disconnect_remote_device_future,);

    'outer: loop {
        let m: RemoteDeviceMessage = futures::select! {
            res = connect_mdpws_device_future => {
                process_connect_mdpws_result(res, &connected_devices, &channel).await;
                continue
            }
            res = disconnect_remote_device_future => {
                match res {
                    Ok(device_id) => {
                        let event = RemoteDeviceEvent::RemoteDeviceDisconnected {
                            device_id: device_id,
                        };
                        send_log_error(event, &channel).await;
                    },
                    Err(err) => {
                        error!("Disconnect failed: {:?}", err)
                    },
                };
                continue
            }
            m = r.recv().fuse() => {
                match m {
                    None => return Ok(()),
                    Some(m) => m,
                }
            }
        };

        debug!("{:?}", m);

        match m {
            RemoteDeviceMessage::Connect {
                device_id,
                device_epr,
                adapter_ip,
                protocol,
                crypto,
            } => match protocol {
                Protocol::MDPWS => connect_mdpws_device_future
                    .set(connect_dpws_device(adapter_ip, device_epr, device_id, crypto).fuse()),
                Protocol::PROTOSDC => {}
            },
            RemoteDeviceMessage::Disconnect { device_id } => {
                if let Some(device) = connected_devices.lock().await.remove(&device_id) {
                    let event = RemoteDeviceEvent::RemoteDeviceDisconnecting { device_id };
                    send_log_error(event, &channel).await;
                    disconnect_remote_device_future.set(disconnect_device(device).fuse())
                }
            }
            RemoteDeviceMessage::SubscribeDevice {
                response,
                device_id,
            } => {
                if let Some(device) = connected_devices.lock().await.get(&device_id) {
                    let mut receiver = device.broadcast_sender.subscribe();
                    let evt = match &device.remote_device {
                        RemoteDevice::MDPWS(i) => {
                            initial_update_from_mdib(&i.remote_mdib, device_id.clone()).await
                        }
                        RemoteDevice::PROTOSDC(i) => {
                            let mdib = i.get_mdib_access().await;
                            initial_update_from_mdib(&mdib, device_id.clone()).await
                        }
                    };
                    let mut evt_overflow = vec![];

                    // drop updates before evt to transmit a consistent mdib in this sender
                    // if we peek and there is an applicable element, put it into the overflow and
                    // send it attached to the initial mdib
                    'inner: loop {
                        match receiver.try_recv() {
                            // we got a message, we need to process message until we either find
                            // the first update we care about or no more updates
                            Ok(msg) => {
                                // don't compare between devices, bad mojo (and also errors)
                                if msg.device_id == device_id {
                                    match msg.mdib_version.cmp_to(&evt.mdib_version) {
                                        None => {
                                            error!(
                                                "Cannot compare mdib versions of update and mdib"
                                            );
                                            continue 'outer;
                                        }
                                        Some(Ordering::Less) => {} // continue
                                        Some(Ordering::Equal) => {} // continue
                                        Some(Ordering::Greater) => {
                                            evt_overflow.push(msg);
                                            break 'inner;
                                        }
                                    }
                                }
                            }
                            // easy case, nothing to evaluate, we're good
                            Err(TryRecvError::Empty) => break 'inner,
                            Err(e) => {
                                error!("Error peeking, aborting request: {:?}", e);
                                continue 'outer;
                            }
                        }
                    }

                    // prepend the initial update to restore order
                    evt_overflow.insert(0, evt);

                    oneshot_log_error((evt_overflow, receiver), response);
                }
            }
            RemoteDeviceMessage::Quit => return Ok(()),
            RemoteDeviceMessage::GetConnectedDevices { response } => {
                let connected = connected_devices
                    .lock()
                    .await
                    .iter()
                    .map(|(device_id, remote_device)| RemoteDeviceFrontend {
                        device_id: device_id.clone(),
                        adapter_ip: remote_device.adapter_ip.clone(),
                        protocol: match remote_device.remote_device {
                            RemoteDevice::MDPWS(_) => Protocol::MDPWS,
                            RemoteDevice::PROTOSDC(_) => Protocol::PROTOSDC,
                        },
                    })
                    .collect::<Vec<_>>();

                oneshot_log_error(connected, response);
            }
            RemoteDeviceMessage::GetRawMdib {
                device_id,
                response,
            } => {
                if let Some(device) = connected_devices.lock().await.get(&device_id) {
                    let mdib = match &device.remote_device {
                        RemoteDevice::MDPWS(remote_device) => {
                            let hosting = remote_device.hosting_service.lock().await;
                            if let Ok(mdib_response) = get_mdpws_mdib(&*hosting).await {
                                oneshot_log_error(RawMdibResponse::MDPWS(mdib_response), response);
                            };
                        }
                        RemoteDevice::PROTOSDC(_) => {
                            todo!()
                        }
                    };

                    // oneshot_log_error(connected, response);
                }
            }
        }
    }
}

async fn connect_dpws_device(
    adapter_ip: IpAddr,
    device_epr_addr: String,
    device_id: String,
    // oneshot: oneshot::Sender<(RemoteDeviceData, RemoteDeviceUpdateReceiver)>,
    crypto: Option<DeviceCertificateConfig>,
) -> Result<MdpwsConnectContainer, (String, anyhow::Error)> {
    _connect_dpws_device(adapter_ip, device_epr_addr, device_id.clone(), crypto)
        .await
        .map_err(|err| (device_id, err))
}

async fn _connect_dpws_device(
    adapter_ip: IpAddr,
    device_epr_addr: String,
    device_id: String,
    // oneshot: oneshot::Sender<(RemoteDeviceData, RemoteDeviceUpdateReceiver)>,
    crypto: Option<DeviceCertificateConfig>,
) -> anyhow::Result<MdpwsConnectContainer> {
    let udp_binding = create_udp_binding(adapter_ip, Some(WS_DISCOVERY_PORT)).await?;
    let mut discovery_consumer = DpwsDiscoveryConsumerImpl::new(udp_binding);
    discovery_consumer.start().await?;

    let c = match crypto {
        None => None,
        Some(it) => Some(it.into()),
    };

    let http_client = ReqwestHttpClient::new(c.clone())?;
    let registry = AxumRegistry::new(c);

    let consumer = ConsumerImpl::new(
        discovery_consumer,
        http_client.get_access(),
        registry.access(),
    );
    let hosting_service = consumer.connect(&device_epr_addr).await?;

    info!("{:?}", hosting_service.this_model());
    info!("{:?}", hosting_service.types());

    let connect_configuration = ConnectConfiguration::new(
        ALL_EPISODIC_AND_WAVEFORM_REPORTS
            .iter()
            .map(|it| it.to_string())
            .collect(),
        Vec::with_capacity(0),
    )?;

    let consumer_config = ConsumerConfigBuilder::default().build();

    // subscribe to episodic reports, find the correct hosted service first
    let remote_device: MdpwsSdcRemoteDevice =
        SdcRemoteDeviceConnectorImpl::connect::<
            _,
            _,
            _,
            _,
            ScoController<DpwsSetServiceHandler<ReqwestSoapClient>>,
            ScoTransactionImpl,
        >(hosting_service, connect_configuration, consumer_config)
        .await?;

    Ok(MdpwsConnectContainer {
        device_id,
        remote_device,
        adapter_ip: adapter_ip.to_string(),
    })
}

async fn disconnect_device(
    remote_device_container: RemoteDeviceContainer,
) -> anyhow::Result<String> {
    debug!("Disconnecting {}", remote_device_container.device_id);

    // shutdown change forwarding task
    remote_device_container.subscription_task.abort();

    match remote_device_container.remote_device {
        RemoteDevice::MDPWS(mut mdpws_device) => {
            mdpws_device.disconnect().await?;
            // TODO Move to disconnect
            mdpws_device.watchdog.stop().await?;
        }
        RemoteDevice::PROTOSDC(_) => {}
    }

    Ok(remote_device_container.device_id)
}

async fn initial_update_from_mdib<MDIB: RemoteMdibAccess>(
    remote_mdib: &MDIB,
    device_id: String,
) -> MdibUpdate {
    // TODO: This needs a read transaction so we can determine the mdib version immediately
    let all_entities = remote_mdib.entities_by_type(|_| true).await;
    let mdib_version_from_entities = all_entities
        .iter()
        .map(|it| it.last_changed.clone())
        .reduce(|a, b| match a.cmp_to(&b) {
            None => a,
            Some(Ordering::Equal) => a,
            Some(Ordering::Greater) => a,
            Some(Ordering::Less) => b,
        });

    let mdib_version = match mdib_version_from_entities {
        None => remote_mdib.mdib_version().await,
        Some(x) => x,
    };

    let transformed_entities = all_entities
        .into_iter()
        .map(|it| entity_to_pair_holder(it))
        .collect_vec();

    MdibUpdate {
        device_id,
        mdib_version,
        updates: vec![MdibChange::Mdib {
            pairs: transformed_entities,
        }],
    }
}

pub async fn get_mdpws_mdib<H, HOSTED>(hosting_service: &H) -> Result<String, anyhow::Error>
where
    H: HostingService<HOSTED, ReqwestSoapClient, ReqwestSoapClient>,
    HOSTED: HostedService<ReqwestSoapClient>,
{
    let (_, get_hosted) =
        find_hosted_service_for_type(BICEPS_PORT_TYPE_GET, hosting_service).await?;

    // retrieve raw mdib
    let soap_msg = SoapMessage {
        header: create_header_for_action(
            BICEPS_ACTION_GET_MDIB,
            get_hosted.epr().address.value.as_str(),
        ),
    };
    let get_mdib_body = GetMdib {
        abstract_get: AbstractGet {
            extension_element: None,
        },
    };

    let mut writer = XmlWriter::new(Vec::new());
    soap_msg
        .to_xml_complex(&mut writer, get_mdib_body)
        .map_err(|err| {
            error!("This should be impossible, but GetMdib could not be serialized");
            HttpClientError::NotResponding
        })?;
    let data: Bytes = writer.into_inner().into();

    let sc: &ReqwestSoapClient = get_hosted.soap_client();

    let response = sc.send_soap_request(data).await?;

    let (parts, body) = response.into_parts();

    let get_mdib_response = std::str::from_utf8(body.as_ref())?.to_string();

    Ok(get_mdib_response)
}

async fn find_hosted_service_for_type<'a, H, HOSTED>(
    qualified_name: ConstQualifiedName<'static>,
    hosting_service: &'a H,
) -> Result<(String, &'a HOSTED), SdcRemoteDeviceConnectorError>
where
    H: HostingService<HOSTED, ReqwestSoapClient, ReqwestSoapClient>,
    HOSTED: HostedService<ReqwestSoapClient> + 'a,
{
    hosting_service
        .hosted_services()
        .iter()
        .find(|(_service_id, service)| {
            service
                .service_type()
                .types
                .list
                .list
                .iter()
                .any(|it| it == &qualified_name)
        })
        .map(|it| (it.0.clone(), it.1))
        .ok_or(SdcRemoteDeviceConnectorError::RequiredServicesMissing {
            services: vec![qualified_name.into()],
        })
}
