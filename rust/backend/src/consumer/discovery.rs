use std::net::IpAddr;

use common::Service;
use dpws::discovery::constants::WS_DISCOVERY_PORT;
use dpws::discovery::consumer::discovery_consumer::{
    DpwsDiscoveryConsumer, DpwsDiscoveryConsumerImpl, DpwsDiscoveryEvent,
};
use dpws::xml::parsing::discovery::ProbeMatch;
use futures::{FutureExt, stream, StreamExt};
use futures::future::Fuse;
use log::{error, info};
use network::udp_binding::{create_udp_binding, PROTOSDC_MULTICAST_PORT};
use protosdc::discovery::consumer::discovery_consumer::{
    DiscoveryConsumer, DiscoveryConsumerImpl, DiscoveryEvent,
};
use tokio::sync::mpsc;
use tokio_stream::wrappers::BroadcastStream;

use api::org::protosdc::ui::model::backend::discovery::BookkeeperDevice;
use api::org::protosdc::ui::model::common::{ConnectionState, Protocol as ApiProtocol};

use crate::common::metadata::Protocol;
use crate::common::util::send_log_error;
use crate::consumer::device_bookkeeper::DeviceState;
use crate::consumer::events::DiscoveryStateEvent;
use crate::FrontendDiscoveryEvent;

#[derive(Clone, Debug, serde::Serialize, PartialEq)]
pub struct DiscoveredDevice {
    pub scopes: Vec<String>,
    pub types: Vec<String>,
    pub endpoint_reference_address: String,
    // there is no way around it, this must be a proper http uri
    pub x_addrs: Vec<String>,
    pub protocol: Protocol,
}

impl DiscoveredDevice {
    pub fn to_api(self, device_id: String, device_state: DeviceState) -> BookkeeperDevice {
        let x: ConnectionState = device_state.into();
        BookkeeperDevice {
            device_id,
            endpoint_reference_address: self.endpoint_reference_address,
            protocol: match self.protocol {
                Protocol::MDPWS => ApiProtocol::Mdpws.into(),
                Protocol::PROTOSDC => ApiProtocol::Protosdc.into(),
            },
            scopes: self.scopes,
            types: self.types,
            x_addrs: self.x_addrs,
            connection_state: x.into(),
        }
    }
}

impl From<ProbeMatch> for DiscoveredDevice {
    fn from(value: ProbeMatch) -> Self {
        Self {
            scopes: value
                .probe_match
                .scopes
                .map(|scopes| scopes.scopes.list)
                .unwrap_or(vec![]),
            types: value
                .probe_match
                .types
                .map(|types| {
                    types
                        .types
                        .list
                        .into_iter()
                        .map(|l| l.to_string())
                        .collect()
                })
                .unwrap_or(vec![]),
            endpoint_reference_address: value.probe_match.endpoint_reference.address.value,
            x_addrs: value
                .probe_match
                .x_addrs
                .map(|x| x.list.into_iter().map(|l| l.to_string()).collect())
                .unwrap_or(vec![]),
            protocol: Protocol::MDPWS,
        }
    }
}

//noinspection RsUnresolvedReference
impl From<protosdc_proto::discovery::Endpoint> for DiscoveredDevice {
    fn from(value: protosdc_proto::discovery::Endpoint) -> Self {
        Self {
            scopes: value.scope.into_iter().map(|it| it.value).collect(),
            types: vec![],
            endpoint_reference_address: value.endpoint_identifier,
            x_addrs: value
                .physical_address
                .into_iter()
                .map(|it| it.value)
                .collect(),
            protocol: Protocol::PROTOSDC,
        }
    }
}

pub async fn run_multicast_discovery(
    adapter_ip_string: String,
    channel: mpsc::Sender<DiscoveryStateEvent>,
) {
    let adapter_ip = match adapter_ip_string.parse() {
        Ok(ip) => ip,
        Err(err) => {
            error!("Could not run multicast discovery: {:?}", err);
            send_log_error(
                DiscoveryStateEvent::DiscoveryFailed {
                    reason: format!("Could not run multicast discovery: {:?}", err),
                },
                &channel,
            )
            .await;
            return;
        }
    };
    send_log_error(DiscoveryStateEvent::DiscoveryStarted, &channel).await;

    let dpws_discovery = run_dpws_multicast_discovery(adapter_ip, channel.clone());
    let protosdc_discovery = run_protosdc_multicast_discovery(adapter_ip, channel.clone());

    if let Err(err) = dpws_discovery.await {
        error!("error occurred during dpws discovery {:?}", err);
    };
    if let Err(err) = protosdc_discovery.await {
        error!("error occurred during protosdc discovery {:?}", err);
    };
    send_log_error(DiscoveryStateEvent::DiscoveryFinished, &channel).await;
}

async fn run_dpws_multicast_discovery(
    adapter_ip: IpAddr,
    channel: mpsc::Sender<DiscoveryStateEvent>,
) -> anyhow::Result<Vec<DiscoveredDevice>> {
    let mut discovery_consumer = DpwsDiscoveryConsumerImpl::new(
        create_udp_binding(adapter_ip, Some(WS_DISCOVERY_PORT))
            .await
            .map_err(anyhow::Error::new)?,
    );

    let mut stream = BroadcastStream::new(discovery_consumer.subscribe().await);
    tokio::spawn(async move {
        while let Some(Ok(item)) = stream.next().await {
            match item {
                DpwsDiscoveryEvent::Hello { .. } => {}
                DpwsDiscoveryEvent::Bye { .. } => {}
                DpwsDiscoveryEvent::ProbeMatches(probe_matches) => {
                    stream::iter(probe_matches.probe_match.into_iter())
                        .for_each(|probe_match| async {
                            info!("Sending discovered mdpws device {:?}", &probe_match);
                            send_log_error(
                                DiscoveryStateEvent::DiscoveredDevice(probe_match.into()),
                                &channel,
                            )
                            .await;
                        })
                        .await;
                }
                DpwsDiscoveryEvent::SearchTimeout { .. } => {}
            }
        }
    });

    discovery_consumer.start().await?;
    let result = discovery_consumer.probe(&[], &[], None, None).await?;

    discovery_consumer.stop().await?;

    info!("Found devices {:?}", result);

    let converted = result
        .into_iter()
        .flat_map(|it| it.probe_match.into_iter().map(|pm| pm.into()))
        .collect();

    Ok(converted)
}

async fn run_protosdc_multicast_discovery(
    adapter_ip: IpAddr,
    channel: mpsc::Sender<DiscoveryStateEvent>,
) -> anyhow::Result<Vec<DiscoveredDevice>> {
    let mut discovery_consumer = DiscoveryConsumerImpl::new(
        create_udp_binding(adapter_ip, Some(PROTOSDC_MULTICAST_PORT))
            .await
            .map_err(anyhow::Error::new)?,
        None,
        None,
    );

    let mut stream = BroadcastStream::new(discovery_consumer.subscribe().await);
    tokio::spawn(async move {
        while let Some(Ok(item)) = stream.next().await {
            match item {
                DiscoveryEvent::EndpointEntered { .. } => {}
                DiscoveryEvent::EndpointLeft { .. } => {}
                DiscoveryEvent::SearchMatch {
                    search_id: _,
                    endpoints,
                    proxy: _,
                    secure: _,
                } => {
                    stream::iter(endpoints.into_iter())
                        .for_each(|ep| async {
                            info!("Sending discovered protosdc device {:?}", &ep);
                            send_log_error(
                                DiscoveryStateEvent::DiscoveredDevice(ep.into()),
                                &channel,
                            )
                            .await;
                        })
                        .await;
                }
                DiscoveryEvent::SearchTimeout { .. } => {}
            }
        }
    });

    discovery_consumer.start().await?;
    let result = discovery_consumer.search(vec![], None, None).await?;

    discovery_consumer.stop().await?;

    info!("Found devices {:?}", result);

    let converted = result.into_iter().map(|it| it.into()).collect();

    Ok(converted)
}

pub async fn run_discovery_loop(
    mut discovery_event_input: mpsc::Receiver<FrontendDiscoveryEvent>,
    output_tx: mpsc::Sender<DiscoveryStateEvent>,
) {
    let run_discovery_future = Fuse::terminated();
    futures::pin_mut!(run_discovery_future);

    loop {
        let m: FrontendDiscoveryEvent = futures::select! {
            _res = run_discovery_future => {
                info!("Discovery finished");
                continue
            }
            m = discovery_event_input.recv().fuse() => {
                match m {
                    None => return,
                    Some(m) => m,
                }
            }
        };

        match m {
            FrontendDiscoveryEvent::StartDiscovery { adapter_ip } => {
                run_discovery_future
                    .set(run_multicast_discovery(adapter_ip, output_tx.clone()).fuse());
            }
            FrontendDiscoveryEvent::StopDiscovery => run_discovery_future.set(Fuse::terminated()),
        }
    }
}
