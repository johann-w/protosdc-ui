use biceps::common::mdib_version::MdibVersion;
use log::debug;
use protosdc_biceps::biceps::{
    ActivateOperationState, AlertConditionState, AlertSignalState, AlertSystemState, BatteryState,
    ChannelState, ClockState, EnsembleContextState, EnumStringMetricState,
    LimitAlertConditionState, LocationContextState, Mdib, MdibVersionGroup, MdsState,
    MeansContextState, NumericMetricState, OperatorContextState, PatientContextState,
    RealTimeSampleArrayMetricState, ScoState, SetAlertStateOperationState,
    SetComponentStateOperationState, SetContextStateOperationState, SetMetricStateOperationState,
    SetStringOperationState, SetValueOperationState, StringMetricState, SystemContextState,
    VmdState, WorkflowContextState,
};
use tokio::sync::{broadcast, oneshot};

use api::org::protosdc::ui::model::backend::discovery;
use api::org::protosdc::ui::model::backend::discovery::bookkeeper_event;
use api::org::protosdc::ui::model::backend::mdib::{
    alert_update, AlertUpdate as ApiAlertUpdate, AlertUpdates as ApiAlertUpdates, component_update, ComponentUpdate as ApiComponentUpdate, ComponentUpdates as ApiComponentUpdates,
    context_update, ContextChange as ApiContextChange,
    ContextUpdate as ApiContextUpdate, ContextUpdates as ApiContextUpdates,
    Mdib as ApiMdib, mdib_update,
    MdibUpdate as ApiMdibUpdate, MdibUpdateEvent, metric_update,
    MetricUpdate as ApiMetricUpdate, MetricUpdates as ApiMetricUpdates, operation_update,
    OperationUpdate as ApiOperationUpdate, OperationUpdates as ApiOperationUpdates,
};
use api::org::protosdc::ui::model::backend::remote_device::{
    remote_device_event, RemoteDeviceConnected, RemoteDeviceConnectFailed, RemoteDeviceConnecting,
    RemoteDeviceDisconnected, RemoteDeviceDisconnecting, RemoteDeviceEvent as ApiRemoteDeviceEvent,
};
use api::org::protosdc::ui::model::frontend::{GetRawMdibResponse, raw_mdib, RawMdib};

use crate::common::crypto::DeviceCertificateConfig;
use crate::consumer::device_bookkeeper::DeviceState;
use crate::consumer::discovery::DiscoveredDevice;
use crate::consumer::remote_device_container::{Metadata, PairHolder, RemoteDeviceFrontend};

#[derive(Clone, Debug)]
pub enum BookkeeperEvent {
    // add or update
    DeviceDiscoveryUpdate {
        device_id: String,
        discovery: DiscoveredDevice,
    },
    DevicesRemoved {
        device_ids: Vec<String>,
    },

    DiscoveryStarted,
    DiscoveryFinished,
    DiscoveryFailed {
        reason: String,
    },
}

#[derive(Clone, Debug)]
pub enum RawMdibResponse {
    MDPWS(String),
    PROTOSDC(Mdib),
}

impl Into<GetRawMdibResponse> for RawMdibResponse {
    fn into(self) -> GetRawMdibResponse {
        GetRawMdibResponse {
            raw_mdib: Some(RawMdib {
                mdib: Some(match self {
                    RawMdibResponse::MDPWS(mdib_string) => raw_mdib::Mdib::Mdpws(mdib_string),
                    RawMdibResponse::PROTOSDC(mdib_proto) => {
                        raw_mdib::Mdib::Protosdc(mdib_proto.into())
                    }
                }),
            }),
        }
    }
}

impl Into<discovery::BookkeeperEvent> for BookkeeperEvent {
    fn into(self) -> discovery::BookkeeperEvent {
        match self {
            BookkeeperEvent::DeviceDiscoveryUpdate {
                device_id,
                discovery,
            } => discovery::BookkeeperEvent {
                event: Some(bookkeeper_event::Event::DeviceDiscoveryUpdate(
                    discovery::DeviceDiscoveryUpdate {
                        device: Some(discovery.to_api(device_id, DeviceState::Discovered)),
                    },
                )),
            },
            BookkeeperEvent::DevicesRemoved { device_ids } => discovery::BookkeeperEvent {
                event: Some(bookkeeper_event::Event::DevicesRemoved(
                    discovery::DevicesRemoved { device_ids },
                )),
            },
            BookkeeperEvent::DiscoveryStarted => discovery::BookkeeperEvent {
                event: Some(bookkeeper_event::Event::DiscoveryStarted(
                    discovery::DiscoveryStarted {},
                )),
            },
            BookkeeperEvent::DiscoveryFinished => discovery::BookkeeperEvent {
                event: Some(bookkeeper_event::Event::DiscoveryFinished(
                    discovery::DiscoveryFinished {},
                )),
            },
            BookkeeperEvent::DiscoveryFailed { reason } => discovery::BookkeeperEvent {
                event: Some(bookkeeper_event::Event::DiscoveryFailed(
                    discovery::DiscoveryFailed { reason },
                )),
            },
        }
    }
}

#[derive(Clone, Debug)]
pub enum FrontendDiscoveryEvent {
    StartDiscovery { adapter_ip: String },
    StopDiscovery,
}

#[derive(Debug)]
pub enum FrontendCommand {
    Connect {
        device_id: String,
        adapter_ip: String,
        crypto: Option<DeviceCertificateConfig>,
    },
    Disconnect {
        device_id: String,
    },
    Quit,
    GetConnectedDevices {
        response: oneshot::Sender<Vec<RemoteDeviceFrontend>>,
    },
    SubscribeBookkeeper {
        response: oneshot::Sender<(Vec<BookkeeperEvent>, broadcast::Receiver<BookkeeperEvent>)>,
    },
    SubscribeDevice {
        device_id: String,
        response: oneshot::Sender<(Vec<MdibUpdate>, broadcast::Receiver<MdibUpdate>)>,
    },
    GetRawMdib {
        device_id: String,
        response: oneshot::Sender<RawMdibResponse>,
    },
}

#[derive(Clone, Debug, serde::Serialize)]
pub enum RemoteDeviceEvent {
    RemoteDeviceConnecting {
        device_id: String,
    },
    RemoteDeviceConnected {
        device_id: String,
        metadata: Metadata,
    },
    RemoteDeviceConnectFailed {
        device_id: String,
        reason: String,
    },
    RemoteDeviceDisconnecting {
        device_id: String,
    },
    RemoteDeviceDisconnected {
        device_id: String,
    },
}

impl Into<ApiRemoteDeviceEvent> for RemoteDeviceEvent {
    fn into(self) -> ApiRemoteDeviceEvent {
        let event = match self {
            RemoteDeviceEvent::RemoteDeviceConnecting { device_id } => {
                remote_device_event::Event::Connecting(RemoteDeviceConnecting { device_id })
            }
            RemoteDeviceEvent::RemoteDeviceConnected {
                device_id,
                metadata,
            } => remote_device_event::Event::Connected(RemoteDeviceConnected {
                device_id,
                metadata: Some(metadata.device_metadata.into()),
            }),
            RemoteDeviceEvent::RemoteDeviceConnectFailed { device_id, reason } => {
                remote_device_event::Event::Failed(RemoteDeviceConnectFailed { device_id, reason })
            }
            RemoteDeviceEvent::RemoteDeviceDisconnecting { device_id } => {
                remote_device_event::Event::Disconnecting(RemoteDeviceDisconnecting { device_id })
            }
            RemoteDeviceEvent::RemoteDeviceDisconnected { device_id } => {
                remote_device_event::Event::Disconnected(RemoteDeviceDisconnected { device_id })
            }
        };

        ApiRemoteDeviceEvent { event: Some(event) }
    }
}

#[derive(Clone, Debug)]
pub enum MetricUpdate {
    NumericMetric {
        state: NumericMetricState,
    },
    StringMetric {
        state: StringMetricState,
    },
    EnumStringMetric {
        state: EnumStringMetricState,
    },
    RealTimeSampleArrayMetric {
        state: RealTimeSampleArrayMetricState,
    },
}

#[derive(Clone, Debug)]
pub enum ComponentUpdate {
    Battery { state: BatteryState },
    Channel { state: ChannelState },
    Clock { state: ClockState },
    Sco { state: ScoState },
    SystemContext { state: SystemContextState },
    Mds { state: MdsState },
    Vmd { state: VmdState },
}

#[derive(Clone, Debug)]
pub enum AlertUpdate {
    AlertCondition { state: AlertConditionState },
    AlertSignal { state: AlertSignalState },
    AlertSystem { state: AlertSystemState },
    LimitAlertCondition { state: LimitAlertConditionState },
}

#[derive(Clone, Debug)]
pub enum ContextStateUpdate {
    Ensemble { state: EnsembleContextState },
    Location { state: LocationContextState },
    Means { state: MeansContextState },
    Operator { state: OperatorContextState },
    Patient { state: PatientContextState },
    Workflow { state: WorkflowContextState },
}

#[derive(Clone, Debug)]
pub enum OperationUpdate {
    ActivateOperation {
        state: ActivateOperationState,
    },
    SetAlertStateOperation {
        state: SetAlertStateOperationState,
    },
    SetComponentStateOperation {
        state: SetComponentStateOperationState,
    },
    SetContextStateOperation {
        state: SetContextStateOperationState,
    },
    SetMetricStateOperation {
        state: SetMetricStateOperationState,
    },
    SetStringOperation {
        state: SetStringOperationState,
    },
    SetValueOperation {
        state: SetValueOperationState,
    },
}

#[derive(Clone, Debug)]
pub enum ContextUpdateKind {
    InsertUpdate,
    Delete,
}

#[derive(Clone, Debug)]
pub struct ContextUpdate {
    pub kind: ContextUpdateKind,
    pub update: ContextStateUpdate,
}

#[derive(Clone, Debug)]
pub struct MetricUpdateEvent {
    pub metric_update: Vec<MetricUpdate>,
}

#[derive(Clone, Debug)]
pub struct ComponentUpdateEvent {
    pub component_update: Vec<ComponentUpdate>,
}

#[derive(Clone, Debug)]
pub struct ContextUpdateEvent {
    pub context_update: Vec<ContextUpdate>,
}

#[derive(Clone, Debug)]
pub struct OperationUpdateEvent {
    pub operation_update: Vec<OperationUpdate>,
}

#[derive(Clone, Debug)]
pub struct AlertUpdateEvent {
    pub alert_update: Vec<AlertUpdate>,
}

#[derive(Clone, Debug)]
pub enum MdibChange {
    MetricUpdate(MetricUpdateEvent),
    ComponentUpdate(ComponentUpdateEvent),
    ContextUpdate(ContextUpdateEvent),
    OperationUpdate(OperationUpdateEvent),
    AlertUpdates(AlertUpdateEvent),
    Mdib { pairs: Vec<PairHolder> },
}

#[derive(Clone, Debug)]
pub struct MdibUpdate {
    pub device_id: String,
    pub mdib_version: MdibVersion,
    pub updates: Vec<MdibChange>,
}

#[derive(Clone, Debug, serde::Serialize)]
pub enum DiscoveryStateEvent {
    DiscoveryStarted,
    DiscoveredDevice(DiscoveredDevice),
    DiscoveryFinished,
    DiscoveryFailed { reason: String },
}

impl Into<MdibUpdateEvent> for MdibUpdate {
    fn into(self) -> MdibUpdateEvent {
        let grp: MdibVersionGroup = self.mdib_version.into();
        MdibUpdateEvent {
            device_id: self.device_id,
            mdib_updates: self.updates.into_iter().map(|it| it.into()).collect(),
            mdib_version: Some(grp.into()),
        }
    }
}

impl Into<ApiContextChange> for ContextUpdateKind {
    fn into(self) -> ApiContextChange {
        match self {
            ContextUpdateKind::InsertUpdate => ApiContextChange::InsertUpdate,
            ContextUpdateKind::Delete => ApiContextChange::Delete,
        }
    }
}

impl Into<ApiMdibUpdate> for MdibChange {
    fn into(self) -> ApiMdibUpdate {
        let mdib_update_state = match self {
            MdibChange::MetricUpdate(change) => {
                let updates = change
                    .metric_update
                    .into_iter()
                    .map(|it| match it {
                        MetricUpdate::NumericMetric { state, .. } => {
                            metric_update::State::NumericMetricStateMsg(state.into())
                        }
                        MetricUpdate::StringMetric { state, .. } => {
                            metric_update::State::StringMetricStateMsg(state.into())
                        }
                        MetricUpdate::EnumStringMetric { state, .. } => {
                            metric_update::State::EnumStringMetricStateMsg(state.into())
                        }
                        MetricUpdate::RealTimeSampleArrayMetric { state, .. } => {
                            metric_update::State::RealTimeSampleArrayMetricStateMsg(state.into())
                        }
                    })
                    .map(|it| ApiMetricUpdate { state: Some(it) })
                    .collect::<Vec<_>>();

                let metric_update = ApiMetricUpdates {
                    metric_updates: updates,
                };

                mdib_update::State::MetricUpdates(metric_update)
            }
            MdibChange::ComponentUpdate(component_update) => {
                let updates = component_update
                    .component_update
                    .into_iter()
                    .map(|it| match it {
                        ComponentUpdate::Battery { state } => {
                            component_update::State::BatteryStateMsg(state.into())
                        }
                        ComponentUpdate::Channel { state } => {
                            component_update::State::ChannelStateMsg(state.into())
                        }
                        ComponentUpdate::Clock { state } => {
                            component_update::State::ClockStateMsg(state.into())
                        }
                        ComponentUpdate::Sco { state } => {
                            component_update::State::ScoStateMsg(state.into())
                        }
                        ComponentUpdate::SystemContext { state } => {
                            component_update::State::SystemContextStateMsg(state.into())
                        }
                        ComponentUpdate::Mds { state } => {
                            component_update::State::MdsStateMsg(state.into())
                        }
                        ComponentUpdate::Vmd { state } => {
                            component_update::State::VmdStateMsg(state.into())
                        }
                    })
                    .map(|it| ApiComponentUpdate { state: Some(it) })
                    .collect::<Vec<_>>();

                mdib_update::State::ComponentUpdates(ApiComponentUpdates {
                    component_updates: updates,
                })
            }
            MdibChange::ContextUpdate(context_update) => {
                let updates = context_update
                    .context_update
                    .into_iter()
                    .map(|it| {
                        let update = match it.update {
                            ContextStateUpdate::Ensemble { state } => {
                                context_update::State::EnsembleContextStateMsg(state.into())
                            }
                            ContextStateUpdate::Location { state } => {
                                context_update::State::LocationContextStateMsg(state.into())
                            }
                            ContextStateUpdate::Means { state } => {
                                context_update::State::MeansContextStateMsg(state.into())
                            }
                            ContextStateUpdate::Operator { state } => {
                                context_update::State::OperatorContextStateMsg(state.into())
                            }
                            ContextStateUpdate::Patient { state } => {
                                context_update::State::PatientContextStateMsg(state.into())
                            }
                            ContextStateUpdate::Workflow { state } => {
                                context_update::State::WorkflowContextStateMsg(state.into())
                            }
                        };
                        let change: ApiContextChange = it.kind.into();
                        ApiContextUpdate {
                            context_change: change.into(),
                            state: Some(update),
                        }
                    })
                    .collect::<Vec<_>>();

                mdib_update::State::ContextUpdates(ApiContextUpdates {
                    context_updates: updates,
                })
            }
            MdibChange::OperationUpdate(operation_update) => {
                let updates = operation_update
                    .operation_update
                    .into_iter()
                    .map(|it| match it {
                        OperationUpdate::ActivateOperation { state } => {
                            operation_update::State::ActivateOperationState(state.into())
                        }
                        OperationUpdate::SetAlertStateOperation { state } => {
                            operation_update::State::SetAlertStateOperationState(state.into())
                        }
                        OperationUpdate::SetComponentStateOperation { state } => {
                            operation_update::State::SetComponentStateOperationState(state.into())
                        }
                        OperationUpdate::SetContextStateOperation { state } => {
                            operation_update::State::SetContextStateOperationState(state.into())
                        }
                        OperationUpdate::SetMetricStateOperation { state } => {
                            operation_update::State::SetMetricStateOperationState(state.into())
                        }
                        OperationUpdate::SetStringOperation { state } => {
                            operation_update::State::SetStringOperationState(state.into())
                        }
                        OperationUpdate::SetValueOperation { state } => {
                            operation_update::State::SetValueOperationState(state.into())
                        }
                    })
                    .map(|it| ApiOperationUpdate { state: Some(it) })
                    .collect::<Vec<_>>();

                mdib_update::State::OperationUpdates(ApiOperationUpdates {
                    operation_updates: updates,
                })
            }
            MdibChange::AlertUpdates(alert_update) => {
                let updates = alert_update
                    .alert_update
                    .into_iter()
                    .map(|it| match it {
                        AlertUpdate::AlertCondition { state } => {
                            alert_update::State::AlertConditionStateMsg(state.into())
                        }
                        AlertUpdate::AlertSignal { state } => {
                            alert_update::State::AlertSignalStateMsg(state.into())
                        }
                        AlertUpdate::AlertSystem { state } => {
                            alert_update::State::AlertSystemStateMsg(state.into())
                        }
                        AlertUpdate::LimitAlertCondition { state } => {
                            alert_update::State::LimitAlertConditionStateMsg(state.into())
                        }
                    })
                    .map(|it| ApiAlertUpdate { state: Some(it) })
                    .collect::<Vec<_>>();

                mdib_update::State::AlertUpdates(ApiAlertUpdates {
                    alert_updates: updates,
                })
            }
            MdibChange::Mdib { pairs } => {
                let mut metric_pairs = vec![];
                let mut component_pairs = vec![];
                let mut alert_pairs = vec![];
                let mut context_pairs = vec![];
                let mut operation_pairs = vec![];

                let num_pairs = pairs.len();

                for entry in pairs.into_iter() {
                    match entry {
                        PairHolder::Metric(m) => {
                            metric_pairs.push(m);
                        }
                        PairHolder::Alert(a) => {
                            alert_pairs.push(a);
                        }
                        PairHolder::Component(c) => {
                            component_pairs.push(c);
                        }
                        PairHolder::Context(c) => {
                            context_pairs.push(c);
                        }
                        PairHolder::Operation(o) => {
                            operation_pairs.push(o);
                        }
                    }
                }

                debug!("Sorted {} into {} metrics, {} alerts, {} components, {} contexts, {} operations", num_pairs, metric_pairs.len(), alert_pairs.len(), component_pairs.len(), context_pairs.len(), operation_pairs.len());

                mdib_update::State::Mdib(ApiMdib {
                    metric_pairs,
                    component_pairs,
                    alert_pairs,
                    context_pairs,
                    operation_pairs,
                })
            }
        };
        ApiMdibUpdate {
            state: Some(mdib_update_state),
        }
    }
}
