extern crate walkdir;

use std::path::{Path, PathBuf};
use walkdir::{DirEntry, Error as WalkDirError, WalkDir};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let current_file_dir = "src";

    fn find<P>(root: P, ext: &str) -> Result<Vec<DirEntry>, WalkDirError>
    where
        P: AsRef<Path>,
    {
        let entries: Result<Vec<DirEntry>, WalkDirError> = WalkDir::new(root)
            .follow_links(true)
            .into_iter()
            .filter_entry(|e| {
                let actual_ext = e.path().extension().map(|s| s.to_str());
                e.file_type().is_dir()
                    || e.file_type().is_symlink()
                    || actual_ext == Some(Some(ext))
            })
            .collect();

        // We have to do a second pass on the walked results and filter out dirs,
        // because if we do it on the first pass, walkdir won't recurse into
        // directories
        let entries: Vec<DirEntry> = entries?
            .into_iter()
            .filter(|e| {
                let actual_ext = e.path().extension().map(|s| s.to_str());
                actual_ext == Some(Some(ext))
            })
            .collect();

        Ok(entries)
    }

    let protos: Vec<PathBuf> = find(format!("{}", current_file_dir), "proto")?
        .into_iter()
        .map(|e| e.into_path())
        .collect();
    // find all files in this proto hell

    protos.iter().for_each(|proto| {
        println!("Building proto {:?}", proto);
    });

    let path: PathBuf = format!("{}/", current_file_dir).into();
    let dummies: PathBuf = format!("{}/../dummy_files", current_file_dir).into();

    println!("Including {:?}", path);

    tonic_build::configure()
        .extern_path(
            ".org.somda.protosdc.proto.model.biceps",
            "::protosdc_proto::biceps",
        )
        .extern_path(
            ".org.somda.protosdc.proto.model.common",
            "::protosdc_proto::common",
        )
        .extern_path(
            ".org.somda.protosdc.proto.model.metadata",
            "::protosdc_proto::metadata",
        )
        // .extern_path(".org.somda.protosdc.proto.model.biceps.StringMetricState", "::protosdc_proto::biceps::StringMetricStateMsg")
        // .extern_path(".org.somda.protosdc.proto.model.biceps.NumericMetricState", "::protosdc_proto::biceps::NumericMetricStateMsg")
        // .extern_path(".org.somda.protosdc.proto.model.metadata.EndpointMetadata", "::protosdc_proto::metadata::EndpointMetadata")
        .compile_well_known_types(false)
        .include_file(format!("mod.rs"))
        .compile(&protos.as_slice(), &[dummies, path])?;
    Ok(())
}
